package com.taboor.qms.user.management.controller;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.payload.AddUserFAQPayload;
import com.taboor.qms.core.payload.AddUserFAQTypePayload;
import com.taboor.qms.core.payload.AddUserFeedbackPayload;
import com.taboor.qms.core.payload.ChangePasswordPayload;
import com.taboor.qms.core.payload.EditProfilePayload;
import com.taboor.qms.core.payload.EditProfileVerificationPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.payload.GetManyFileDataPayload;
import com.taboor.qms.core.payload.LoginUserPayload;
import com.taboor.qms.core.payload.RegisterUserPayload;
import com.taboor.qms.core.payload.ResetPasswordPayload;
import com.taboor.qms.core.payload.VerificationCodePayload;
import com.taboor.qms.core.payload.VerifyUserPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAllUserFAQResponse;
import com.taboor.qms.core.response.GetAllUserFAQTypeResponse;
import com.taboor.qms.core.response.GetManyFileDataResponse;
import com.taboor.qms.core.response.GetUserResponse;
import com.taboor.qms.core.response.LoginUserResponse;
import com.taboor.qms.core.response.RegisterUserResponse;
import com.taboor.qms.core.utils.FileDataService;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.user.management.payload.AddUserManualPayload;
import com.taboor.qms.user.management.response.GetUserManualListResponse;
import com.taboor.qms.user.management.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping
@CrossOrigin(origins = "*")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @Autowired
    private FileDataService fileDataService;

    @ApiOperation(value = "Check User Exists in the db.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully get customer"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/user/exists", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    GenStatusResponse checkUserExists(@RequestBody @Valid final String email) throws Exception {
        logger.info("Calling Check User Exists - API");
        return userService.checkUserExistsRest(email);
    }

    @ApiOperation(value = "Get User By User Email.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Used fetched Successfully."),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/user/get/email", produces = "application/json")
    public @ResponseBody
    GetUserResponse getUserByEmail(@RequestBody String email) throws Exception {
        logger.info("Calling Get User By Email - API");
        return userService.getUserByEmailRest(email);
    }

    @ApiOperation(value = "Get User Profile.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Used fetched Successfully."),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/user/profile/get", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    GetUserResponse getUserByEmail() throws Exception {
        logger.info("Calling Get User Profile By Session Token - API");
        return userService.getUserProfile();
    }

    @ApiOperation(value = "Register User in the System.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User registered Successfully."),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/user/register", method = RequestMethod.POST, consumes = {
        "multipart/form-data"}, produces = "application/json")
    public @ResponseBody
    RegisterUserResponse registerUser(
            @RequestParam(value = "RegisterUserPayload", required = true) @Valid final String registerUserPayload,
            @RequestParam(value = "UserProfileImage", required = false) @Valid final MultipartFile profileImage)
            throws Exception {
        logger.info("Calling Register User - API");
        return userService.registerUser(
                GenericMapper.jsonToObjectMapper(registerUserPayload, RegisterUserPayload.class), profileImage);
    }

    @ApiOperation(value = "Update User in the System.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User registered Successfully."),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/user/update", method = RequestMethod.POST, consumes = {
        "multipart/form-data"}, produces = "application/json")
    public @ResponseBody
    RegisterUserResponse update(
            @RequestParam(value = "UpdateUserPayload", required = true) @Valid final String registerUserPayload,
            @RequestParam(value = "UserProfileImage", required = false) @Valid final MultipartFile profileImage)
            throws Exception {
        logger.info("Calling Update User - API");
        return userService.updateUser(GenericMapper.jsonToObjectMapper(registerUserPayload, RegisterUserPayload.class),
                profileImage);
    }

    @ApiOperation(value = "Login User in the system.", response = LoginUserResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User Login Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/user/login", method = RequestMethod.POST)
    public @ResponseBody
    LoginUserResponse loginCustomer(@RequestBody @Valid final LoginUserPayload loginUserPayload,
            HttpServletRequest request) throws Exception, Throwable {
        logger.info("Calling Login User - API");
        loginUserPayload.setIpAddress(request.getRemoteAddr());
        return userService.loginUser(loginUserPayload);
    }

    @ApiOperation(value = "Verify User", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully get customer"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/otp/verify", method = RequestMethod.POST)
    public @ResponseBody
    GenStatusResponse verifyUser(@RequestBody @Valid final VerifyUserPayload verifyUserPayload)
            throws Exception {
        logger.info("Calling Verify User - API");
        return userService.verifyUser(verifyUserPayload);
    }

    @ApiOperation(value = "Verify New Contact/Email of User", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully verified customer"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/otp/verify/new", method = RequestMethod.POST)
    public @ResponseBody
    GenStatusResponse verifyNewUserContact(
            @RequestBody @Valid final EditProfileVerificationPayload verifyUserPayload) throws Exception {
        logger.info("Calling Verify New User Contact/Email - API");
        return userService.verifyUserUpdated(verifyUserPayload);
    }

    @ApiOperation(value = "Send Verification Code to User.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Verification Code sent Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/otp/send", method = RequestMethod.POST)
    public @ResponseBody
    GenStatusResponse sendVerificationCode(
            @RequestBody @Valid final VerificationCodePayload verificationCodePayload) throws Exception, Throwable {
        logger.info("Calling Send OTP - API");
        return userService.sendOTP(verificationCodePayload);
    }

    @ApiOperation(value = "Send Verification Code to New Contact/Email of User.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Verification Code sent Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/otp/send/new", method = RequestMethod.POST)
    public @ResponseBody
    GenStatusResponse sendVerificationCodeToNewContact(
            @RequestBody @Valid final EditProfilePayload payload) throws Exception, Throwable {
        logger.info("Calling Send OTP to new Contact/Email - API");
        return userService.sendOTPUpdated(payload);
    }

    @ApiOperation(value = "Change Password of User.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Password Changed Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/password/change", method = RequestMethod.POST)
    public @ResponseBody
    GenStatusResponse changePassword(
            @RequestBody @Valid final ChangePasswordPayload changePasswordPayload) throws Exception, Throwable {
        logger.info("Calling Change Password - API");
        return userService.changeUserPassword(changePasswordPayload);
    }

    @ApiOperation(value = "Reset Password of User.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Password Reset Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/password/reset", method = RequestMethod.POST)
    public @ResponseBody
    GenStatusResponse resetPassword(
            @RequestBody @Valid final ResetPasswordPayload resetPasswordPayload) throws Exception, Throwable {
        logger.info("Calling Reset Password - API");
        return userService.resetUserPassword(resetPasswordPayload);
    }

    @PostMapping("/password/forget")
    public @ResponseBody
    GenStatusResponse forgetPassword(
            @RequestBody @Valid final ResetPasswordPayload resetPasswordPayload) throws Exception, Throwable {
        logger.info("Calling Forget Password - API");
        return userService.forgetUserPassword(resetPasswordPayload);
    }

    @ApiOperation(value = "Logout User from the system.", response = LoginUserResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User Logout Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/user/logout", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    GenStatusResponse logoutUser() throws Exception, Throwable {
        logger.info("Calling Logout User - API");
        return userService.logoutUser();
    }

    @ApiOperation(value = "Delete User from the system.", response = LoginUserResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User deleted Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/user/delete", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    GenStatusResponse deleteUser() throws Exception, Throwable {
        logger.info("Calling Delete User - API");
        return userService.deleteUser();
    }

    @ApiOperation(value = "Delete User from the system.", response = LoginUserResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User deleted Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/user/delete/id", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    GenStatusResponse deleteUserByUserId(@RequestBody GetByIdPayload payload)
            throws Exception, Throwable {
        logger.info("Calling Delete User - API");
        // 0-UserId
        return userService.deleteUserById(payload.getId());
    }

    @ApiOperation(value = "Fetch User Bank Cards.", response = LoginUserResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User bank card fetched Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/user/bankcard/all", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    GenStatusResponse getUserAllBankCards() throws Exception, Throwable {
        logger.info("Calling Get User All Bank Cards - API");
        return userService.getUserBankCards();
    }

    @ApiOperation(value = "Add User Feedback.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "User Feedback added Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/user/feedback/add", method = RequestMethod.POST)
    public @ResponseBody
    GenStatusResponse addUserFeedback(
            @RequestBody @Valid final AddUserFeedbackPayload addUserFeedbackPayload) throws Exception, Throwable {
        logger.info("Calling Add User Feedback - API");
        return userService.addUserFeedback(addUserFeedbackPayload);
    }

    @ApiOperation(value = "Add Frequently Asked Question Type.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Frequently Asked Question Type added Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/faq/type/add", method = RequestMethod.POST)
    public @ResponseBody
    GenStatusResponse addFAQType(
            @RequestBody @Valid final AddUserFAQTypePayload addUserFAQTypePayload) throws Exception, Throwable {
        logger.info("Calling Add FAQ Type- API");
        return userService.addUserFAQType(addUserFAQTypePayload);
    }

    @ApiOperation(value = "Get All Frequently Asked Question's.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Frequently Asked Question added Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/faq/type/get/all", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    GetAllUserFAQTypeResponse getAllFAQTypes() throws Exception, Throwable {
        logger.info("Calling Get All User FAQs - API");
        return userService.getAllUserFAQTypes();
    }

    @ApiOperation(value = "Add Frequently Asked Question.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Frequently Asked Question added Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/faq/add", method = RequestMethod.POST)
    public @ResponseBody
    GenStatusResponse addFAQ(@RequestBody @Valid final AddUserFAQPayload addFAQPayload)
            throws Exception, Throwable {
        logger.info("Calling Add FAQ - API");
        return userService.addUserFAQ(addFAQPayload);
    }

    @ApiOperation(value = "Get All Frequently Asked Question's.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "All Frequently Asked Question fetched Successfully"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/faq/get/all", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    GetAllUserFAQResponse getAllFAQs() throws Exception, Throwable {
        logger.info("Calling Get All User FAQs - API");
        return userService.getAllUserFAQs();
    }

    @PostMapping("/faq/delete")
    public @ResponseBody
    GenStatusResponse deleteUserFAQ(@RequestBody GetByIdPayload payload)
            throws Exception, Throwable {
        logger.info("Calling Delete User FAQ - API");
        return userService.deleteUserFAQ(payload.getId());
    }

    @RequestMapping(value = "/confirmemailaddress", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    void verifyCustomerEmailAddress(@RequestParam String key, HttpServletResponse response)
            throws Exception, Throwable {
        logger.info("Calling Confirm Email Address - API");
        if (userService.verfiyEmailAddress(key) == true) {
            response.sendRedirect("http://www.google.com.pk");
        } else {
            response.sendRedirect("http://www.yahoo.com");
        }
    }

    @RequestMapping(value = "/manual/add", method = RequestMethod.POST, consumes = {
        "multipart/form-data"}, produces = "application/json")
    public @ResponseBody
    GenStatusResponse addUserManual(
            @RequestParam(value = "AddUserManualPayload", required = true) @Valid final String addUserManualPayload,
            @RequestParam(value = "UserManualFile", required = false) @Valid final MultipartFile userManualFile)
            throws Exception {
        logger.info("Calling Add User Manual - API");
        return userService.addUserManual(
                GenericMapper.jsonToObjectMapper(addUserManualPayload, AddUserManualPayload.class), userManualFile);
    }

    @GetMapping("/manual/get/all")
    public @ResponseBody
    GetUserManualListResponse getAllUserManual() throws Exception, Throwable {
        logger.info("Calling Get All User Manuals - API");
        return userService.getAllUserManual();
    }

    @PostMapping("/manual/delete")
    public @ResponseBody
    GenStatusResponse deleteUserManual(@RequestBody GetByIdPayload payload)
            throws Exception, Throwable {
        logger.info("Calling Delete User Manuals - API");
        return userService.deleteUserManual(payload.getId());
    }

    @PostMapping(value = "/fileData/get")
    public @ResponseBody
    GenStatusResponse getFileData(@RequestBody final String filePath) throws Exception, Throwable {
        if (StringUtils.hasText(filePath)) {
            return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), Base64.getEncoder().encodeToString(
                    fileDataService.fetch(new String(Base64.getDecoder().decode(filePath.getBytes())))));
        } else {
            return new GenStatusResponse(xyzResponseCode.ERROR.getCode(), "Payload is Empty");
        }
    }
    
    @PostMapping(value = "/fileData/get/many")
    public @ResponseBody
    GetManyFileDataResponse getManyFileData(@RequestBody final GetManyFileDataPayload payload) throws Exception, Throwable {
        return fileDataService.fetchMany(payload);
    }
    
        @ApiOperation(value = "Update profile picture of user.", response = GenStatusResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Profile picture updated Successfully."),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @RequestMapping(value = "/user/updateProfilePicture", method = RequestMethod.POST, consumes = {
        "multipart/form-data"}, produces = "application/json")
    public @ResponseBody
    GenStatusResponse updateProfilePicture(
            @RequestParam(value = "profileImage", required = true) @Valid final MultipartFile profileImage)
            throws Exception {
        logger.info("Calling Update profile picture of User - API");
        return userService.updateProfilePicture(profileImage);
    }

}
