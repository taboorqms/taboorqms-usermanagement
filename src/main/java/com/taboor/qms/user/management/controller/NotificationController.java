package com.taboor.qms.user.management.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetUserNotificationPayload;
import com.taboor.qms.core.payload.LoginUserPayload;
import com.taboor.qms.core.payload.PushUserNotificationPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAllUserNotificationsResponse;
import com.taboor.qms.core.response.GetIdListResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.user.management.service.NotificationService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/notification")
@CrossOrigin(origins = "*")
public class NotificationController {

	private static final Logger logger = LoggerFactory.getLogger(NotificationController.class);

	@Autowired
	NotificationService notificationService;

	@ApiOperation(value = "Push User Notification in the system.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Push User Notification Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/push", method = RequestMethod.POST)
	public @ResponseBody GenStatusResponse pushNotification(
			@RequestBody @Valid final PushUserNotificationPayload pushUserNotificationPayload)
			throws Exception, Throwable {
		logger.info("Calling Push User Notification - API");
		return notificationService.pushUserNotication(pushUserNotificationPayload);
	}

	@ApiOperation(value = "Get User UnRead Notifications in the system.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Get User Notifications Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/user/unread", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GetValuesResposne getUnReadNotifications() throws Exception, Throwable {
		logger.info("Calling Get User UnRead Notifications - API");
		return notificationService.getUserUnreadCount();
	}

	@PostMapping(value = "/email/user")
	public @ResponseBody GetAllUserNotificationsResponse getEmailNotifications(
			@RequestBody @Valid final GetUserNotificationPayload getUserNotificationPayload)
			throws Exception, Throwable {
		logger.info("Calling Get User Email Notifications - API");
		return notificationService.getUserEmailNotications(getUserNotificationPayload);
	}

	@ApiOperation(value = "Get User Notifications in the system.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Get User Notifications Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/user/all", method = RequestMethod.POST)
	public @ResponseBody GetAllUserNotificationsResponse getNotifications(
			@RequestBody @Valid final GetUserNotificationPayload getUserNotificationPayload)
			throws Exception, Throwable {
		logger.info("Calling Get User Notifications - API");
		return notificationService.getUserNotications(getUserNotificationPayload);
	}

	@ApiOperation(value = "Update User Notifications read status in the system.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Updated User Notifications Read Status Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/user/update/readstatus", method = RequestMethod.POST)
	public @ResponseBody GetIdListResponse getNotifications(
			@RequestBody @Valid final GetByIdListPayload updateUserNotificationReadStatusPayload)
			throws Exception, Throwable {
		logger.info("Calling Update Read Status of User Notifications - API");
		return notificationService.updateNotificationReadStatus(updateUserNotificationReadStatusPayload);
	}
	
	/// test notification 
	@ApiOperation(value = "Test User Notification in the system.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Test User Notification Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/test", method = RequestMethod.POST)
	public @ResponseBody GenStatusResponse testNotification(
			@RequestBody @Valid final LoginUserPayload loginUserPayload) throws Exception, Throwable {
		logger.info("Calling Test Push User Notification - API");
		return notificationService.testUserNotication(loginUserPayload);
	}

}
