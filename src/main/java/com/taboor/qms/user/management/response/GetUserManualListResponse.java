package com.taboor.qms.user.management.response;

import java.util.List;

import com.taboor.qms.core.model.UserManual;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetUserManualListResponse extends GenStatusResponse {

	private List<UserManual> userManuals;

	public List<UserManual> getUserManuals() {
		return userManuals;
	}

	public void setUserManuals(List<UserManual> userManuals) {
		this.userManuals = userManuals;
	}

	public GetUserManualListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetUserManualListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<UserManual> userManuals) {
		super(applicationStatusCode, applicationStatusResponse);
		this.userManuals = userManuals;
	}

}
