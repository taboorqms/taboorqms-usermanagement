package com.taboor.qms.user.management.payload;

import java.util.List;

import com.taboor.qms.core.model.UserPrivilege;

public class AddUserRolePayload {

	private String roleName;
	private Long roleId;
	private List<UserPrivilege> privileges;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public List<UserPrivilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<UserPrivilege> privileges) {
		this.privileges = privileges;
	}

}
