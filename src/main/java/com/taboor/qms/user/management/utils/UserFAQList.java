package com.taboor.qms.user.management.utils;

import java.util.List;

import com.taboor.qms.core.model.UserFAQ;

public class UserFAQList {

	private List<UserFAQ> userFAQList;

	public UserFAQList(List<UserFAQ> userFAQList) {
		super();
		this.userFAQList = userFAQList;
	}

	public List<UserFAQ> getUserFAQList() {
		return userFAQList;
	}

	public void setUserFAQList(List<UserFAQ> userFAQList) {
		this.userFAQList = userFAQList;
	}

}
