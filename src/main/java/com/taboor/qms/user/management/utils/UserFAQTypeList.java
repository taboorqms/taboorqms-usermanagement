package com.taboor.qms.user.management.utils;

import java.util.ArrayList;
import java.util.List;

import com.taboor.qms.core.model.UserFAQType;

public class UserFAQTypeList {

	private List<UserFAQType> userFAQTypeList;

	public UserFAQTypeList() {
		userFAQTypeList = new ArrayList<>();
	}

	public UserFAQTypeList(List<UserFAQType> userFAQTypeList) {
		super();
		this.userFAQTypeList = userFAQTypeList;
	}

	public List<UserFAQType> getUserFAQTypeList() {
		return userFAQTypeList;
	}

	public void setUserFAQTypeList(List<UserFAQType> userFAQTypeList) {
		this.userFAQTypeList = userFAQTypeList;
	}

}
