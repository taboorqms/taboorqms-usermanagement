package com.taboor.qms.user.management.response;

import java.util.Collection;

import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetUserPrivilegeListResponse extends GenStatusResponse {

	private Collection<UserPrivilege> privileges;
	
	public Collection<UserPrivilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(Collection<UserPrivilege> privileges) {
		this.privileges = privileges;
	}

	public GetUserPrivilegeListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetUserPrivilegeListResponse(int applicationStatusCode, String applicationStatusResponse, Collection<UserPrivilege> privileges) {
		super(applicationStatusCode, applicationStatusResponse);
		this.privileges = privileges;
	}

}
