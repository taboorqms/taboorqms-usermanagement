package com.taboor.qms.user.management.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.payload.AddUserBankCardPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetPaymentMethodResponse;
import com.taboor.qms.core.response.GetUserBankCardResponse;
import com.taboor.qms.user.management.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping
@CrossOrigin(origins = "*")
public class UserPaymentController {

	private static final Logger logger = LoggerFactory.getLogger(UserPaymentController.class);

	@Autowired
	UserService userService;

	@ApiOperation(value = "Get All Payment Methods.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "All Payment Methods fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/paymentmethod/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GetPaymentMethodResponse getAllPaymentMethods() throws Exception, Throwable {
		logger.info("Calling Get All Payment Methods - API");
		return userService.getAllPaymentMethods();
	}

	@ApiOperation(value = "Get Specific Payment Method.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Specific Payment Method fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/paymentmethod/get/id", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetPaymentMethodResponse getPaymentMethod(@RequestBody GetByIdPayload paymentMethodId)
			throws Exception, Throwable {
		logger.info("Calling Get Payment Method - API");
		return userService.getPaymentMethod(paymentMethodId.getId());
	}

	@ApiOperation(value = "Add User Bank Card.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added User Bank Card."),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/bankcard/add", method = RequestMethod.POST)
	public @ResponseBody GenStatusResponse addBankCardInformation(
			@RequestBody @Valid final AddUserBankCardPayload addUserBankCardPayload) throws Exception {
		logger.info("Calling Add User Bank Card - API");
		return userService.addUserBankCard(addUserBankCardPayload);
	}

	@ApiOperation(value = "Update User Bank Card.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully Updated User Bank Card."),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/bankcard/update", method = RequestMethod.POST)
	public @ResponseBody GenStatusResponse updateBankCardInformation(
			@RequestBody @Valid final AddUserBankCardPayload addUserBankCardPayload) throws Exception {
		logger.info("Calling Update User Bank Card - API");
		return userService.updateUserBankCard(addUserBankCardPayload);
	}

	@ApiOperation(value = "Get User Bank Cards.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User Bank Cards fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/bankcard/get/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GetUserBankCardResponse getUserBankCards() throws Exception, Throwable {
		logger.info("Calling Get User Bank Cards - API");
		return userService.getUserBankCards();
	}

	@ApiOperation(value = "Get Specific User Bank Card.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Specific User Bank Card fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/bankcard/get/id", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetUserBankCardResponse getUserBankCard(@RequestBody GetByIdPayload userbankCardId)
			throws Exception, Throwable {
		logger.info("Calling Get User Bank Card - API");
		return userService.getUserBankCard(userbankCardId.getId());
	}

	@ApiOperation(value = "Delete User Bank Card.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User Bank Card deleted Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/bankcard/delete/id", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse deleteUserBankCard(@RequestBody GetByIdPayload userBankCardId)
			throws Exception, Throwable {
		logger.info("Calling Delete User Bank Card - API");
		return userService.deleteUserBankCard(userBankCardId.getId());
	}
}
