package com.taboor.qms.user.management.serviceImpl;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.UserRole;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetUserRoleListResponse;
import com.taboor.qms.core.response.GetUserRoleListResponse.RoleObject;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.RoleName;
import com.taboor.qms.user.management.payload.AddUserRolePayload;
import com.taboor.qms.user.management.response.GetUserPrivilegeListResponse;
import com.taboor.qms.user.management.service.UserRoleService;

@Service
public class UserRoleServiceImpl implements UserRoleService {

	private RestTemplate restTemplate = new RestTemplate();

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Override
	public UserRole getByRoleName(@Valid String roleName) throws TaboorQMSServiceException {
		String uri = dbConnectorMicroserviceURL + "/tab/userroles/get/roleName?roleName=" + roleName;
		ResponseEntity<UserRole> getUserRoleResponse = restTemplate.getForEntity(uri, UserRole.class);
		return getUserRoleResponse.getBody();
	}

	@Override
	public GetUserRoleListResponse getByRoleNameRest(@Valid String roleName) throws TaboorQMSServiceException {
		UserRole userRole = getByRoleName(roleName);
		if (userRole == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorCode() + "::"
					+ xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorDetails());
		return new GetUserRoleListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_ROLE_FETCHED.getMessage(), Arrays.asList(new RoleObject(userRole, null)), 0);
	}

	@Override
	public GetUserRoleListResponse getAll()
			throws TaboorQMSServiceException, JsonGenerationException, JsonMappingException, IOException {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();

		String uri = dbConnectorMicroserviceURL + "/tab/userroles/get/all/2?roleName="
				+ session.getUser().getRoleName();
		ResponseEntity<?> getUserRoleResponse = restTemplate.getForEntity(uri, List.class);
		List<RoleObject> userRoleList = GenericMapper.convertListToObject(getUserRoleResponse.getBody(),
				new TypeReference<List<RoleObject>>() {
				});
		for (RoleObject role : userRoleList)
			role.getRole().setRoleName(role.getRole().getRoleName().replaceAll("_", " "));

		uri = dbConnectorMicroserviceURL + "/tab/userprivileges/count";
		ResponseEntity<Long> countUserPrivilegeResponse = restTemplate.getForEntity(uri, Long.class);
		return new GetUserRoleListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_ROLE_FETCHED.getMessage(), userRoleList,
				countUserPrivilegeResponse.getBody().intValue());
	}

	@Override
	public GetUserPrivilegeListResponse getPrivileges()
			throws TaboorQMSServiceException, JsonGenerationException, JsonMappingException, IOException {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		UserRole userRole = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/userroles/get/roleName?roleName=" + session.getUser().getRoleName(),
				UserRole.class, xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorDetails());
		return new GetUserPrivilegeListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_PRIVILEGE_FETCHED.getMessage(), userRole.getUserPrivileges());
	}

	@Override
	public GenStatusResponse addUserRole(AddUserRolePayload addUserRolePayload) throws TaboorQMSServiceException {
		UserRole userRole = RestUtil.getRequestNoCheck(
				dbConnectorMicroserviceURL + "/tab/userroles/get/roleName?roleName=" + addUserRolePayload.getRoleName(),
				UserRole.class);
		if (userRole != null)
			throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_ALREADY_EXISTS.getErrorCode() + "::"
					+ xyzErrorMessage.USERROLE_ALREADY_EXISTS.getErrorDetails());
		userRole = new UserRole();
		userRole.setRoleName(addUserRolePayload.getRoleName());
		userRole.setUserPrivileges(addUserRolePayload.getPrivileges());

		String uri = dbConnectorMicroserviceURL + "/tab/userroles/save";
		HttpEntity<UserRole> userRoleEntity = new HttpEntity<>(userRole, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<UserRole> createUserRoleResponse = restTemplate.postForEntity(uri, userRoleEntity,
				UserRole.class);

		if (!createUserRoleResponse.getStatusCode().equals(HttpStatus.OK) || createUserRoleResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_NOT_CREATED.getErrorCode() + "::"
					+ xyzErrorMessage.USERROLE_NOT_CREATED.getErrorDetails());
		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_ROLE_ADDED.getMessage());
	}

	@Override
	public GenStatusResponse deleteUserRole(GetByIdPayload getByIdPayload) throws TaboorQMSServiceException {
		String uri = dbConnectorMicroserviceURL + "/tab/userroles/delete?roleId=" + getByIdPayload.getId();
		ResponseEntity<Integer> deleteUserRoleResponse = restTemplate.getForEntity(uri, Integer.class);

		if (deleteUserRoleResponse.getBody() == -1)
			throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_NOT_DELETED.getErrorCode() + "::"
					+ xyzErrorMessage.USERROLE_NOT_DELETED.getErrorDetails()
					+ ". Because this is a default UserRole and it cannot be deleted.");
		if (deleteUserRoleResponse.getBody() > 0)
			throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_NOT_DELETED.getErrorCode() + "::"
					+ xyzErrorMessage.USERROLE_NOT_DELETED.getErrorDetails() + ". Because there are '"
					+ deleteUserRoleResponse.getBody() + "' user(s) assigned to this Role.");

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_ROLE_DELETED.getMessage());
	}

	@Override
	public GenStatusResponse updateUserRole(AddUserRolePayload addUserRolePayload) throws TaboorQMSServiceException {
		UserRole userRole = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/userroles/get/id?roleId=" + addUserRolePayload.getRoleId(),
				UserRole.class, xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorDetails());

		for (RoleName item : RoleName.values())
			if (item.name().equals(userRole.getRoleName()))
				throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_NOT_UPDATED.getErrorCode() + "::"
						+ xyzErrorMessage.USERROLE_NOT_UPDATED.getErrorDetails()
						+ ". Because this is a default UserRole and it cannot be updated.");

//		userRole.setRoleName(addUserRolePayload.getRoleName());
		userRole.setUserPrivileges(addUserRolePayload.getPrivileges());

		String uri = dbConnectorMicroserviceURL + "/tab/userroles/update";
		HttpEntity<UserRole> userRoleEntity = new HttpEntity<>(userRole, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<UserRole> createUserRoleResponse = restTemplate.postForEntity(uri, userRoleEntity,
				UserRole.class);

		if (!createUserRoleResponse.getStatusCode().equals(HttpStatus.OK) || createUserRoleResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_NOT_UPDATED.getErrorCode() + "::"
					+ xyzErrorMessage.USERROLE_NOT_UPDATED.getErrorDetails());
		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_ROLE_ADDED.getMessage());
	}

}
