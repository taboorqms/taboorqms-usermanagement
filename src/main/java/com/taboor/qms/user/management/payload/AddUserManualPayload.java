package com.taboor.qms.user.management.payload;

public class AddUserManualPayload {

	private String name;
	private Long userManualId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getUserManualId() {
		return userManualId;
	}

	public void setUserManualId(Long userManualId) {
		this.userManualId = userManualId;
	}

}
