package com.taboor.qms.user.management.service;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.payload.CreateSessionPayload;
import com.taboor.qms.core.response.CreateSessionResponse;

@Service
public interface SessionService {

	public CreateSessionResponse createUserSession(@Valid CreateSessionPayload createSessionPayload)
			throws TaboorQMSServiceException, Exception;

	public Session getBySessionToken(String sessionToken) throws TaboorQMSServiceException, Exception;

	public CreateSessionResponse refreshSession(String refreshToken) throws TaboorQMSServiceException, Exception;

	Session getByRefreshToken(String refreshToken) throws TaboorQMSServiceException, Exception;

}
