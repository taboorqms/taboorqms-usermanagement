package com.taboor.qms.user.management.service;

import java.io.UnsupportedEncodingException;

import javax.validation.Valid;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.payload.AddUserBankCardPayload;
import com.taboor.qms.core.payload.AddUserFAQPayload;
import com.taboor.qms.core.payload.AddUserFAQTypePayload;
import com.taboor.qms.core.payload.AddUserFeedbackPayload;
import com.taboor.qms.core.payload.ChangePasswordPayload;
import com.taboor.qms.core.payload.EditProfilePayload;
import com.taboor.qms.core.payload.EditProfileVerificationPayload;
import com.taboor.qms.core.payload.LoginUserPayload;
import com.taboor.qms.core.payload.RegisterUserPayload;
import com.taboor.qms.core.payload.ResetPasswordPayload;
import com.taboor.qms.core.payload.VerificationCodePayload;
import com.taboor.qms.core.payload.VerifyUserPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAllUserFAQResponse;
import com.taboor.qms.core.response.GetAllUserFAQTypeResponse;
import com.taboor.qms.core.response.GetPaymentMethodResponse;
import com.taboor.qms.core.response.GetUserBankCardResponse;
import com.taboor.qms.core.response.GetUserResponse;
import com.taboor.qms.core.response.LoginUserResponse;
import com.taboor.qms.core.response.RegisterUserResponse;
import com.taboor.qms.user.management.payload.AddUserManualPayload;
import com.taboor.qms.user.management.response.GetUserManualListResponse;

@Service
public interface UserService {

	public boolean checkUserExists(String email);
	public boolean checkUserExistsPhone(String phoneNumber);

	public User getUserByEmail(String email) throws UnsupportedEncodingException;
	public User getUserByPhoneNumber(String number) throws UnsupportedEncodingException;

	public GenStatusResponse verifyUserUpdated(EditProfileVerificationPayload verifyUserPayload)
			throws TaboorQMSServiceException;

	public GenStatusResponse verifyUser(VerifyUserPayload verifyUserPayload)
			throws TaboorQMSServiceException, UnsupportedEncodingException;

	public GenStatusResponse sendOTP(@Valid VerificationCodePayload verificationCodePayload)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse sendOTPUpdated(@Valid EditProfilePayload payload)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse changeUserPassword(@Valid ChangePasswordPayload changePasswordPayload)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse resetUserPassword(@Valid ResetPasswordPayload resetPasswordPayload)
			throws TaboorQMSServiceException, Exception;

	public RegisterUserResponse registerUser(@Valid RegisterUserPayload registerUserPayload, MultipartFile profileImage)
			throws TaboorQMSServiceException, Exception;

	public GetUserResponse getUserByEmailRest(String email) throws TaboorQMSServiceException, Exception;

	public GetUserResponse getUserProfile() throws TaboorQMSServiceException, Exception;

	public GenStatusResponse checkUserExistsRest(String email) throws TaboorQMSServiceException, Exception;

	public GenStatusResponse logoutUser() throws TaboorQMSServiceException, Exception;

	public GenStatusResponse deleteUser() throws TaboorQMSServiceException, Exception;

	public GenStatusResponse addUserFeedback(@Valid AddUserFeedbackPayload addUserFeedbackPayload)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse addUserFAQ(@Valid AddUserFAQPayload addFAQPayload)
			throws TaboorQMSServiceException, Exception;

	public GetAllUserFAQResponse getAllUserFAQs() throws TaboorQMSServiceException, Exception;

	public GenStatusResponse addUserFAQType(@Valid AddUserFAQTypePayload addUserFAQTypePayload)
			throws TaboorQMSServiceException, Exception;

	public GetAllUserFAQTypeResponse getAllUserFAQTypes() throws TaboorQMSServiceException, Exception;

	public LoginUserResponse loginUser(@Valid LoginUserPayload loginUserPayload)
			throws TaboorQMSServiceException, Exception;

	public GetPaymentMethodResponse getAllPaymentMethods() throws TaboorQMSServiceException, Exception;

	public GetUserBankCardResponse getUserBankCards() throws TaboorQMSServiceException, Exception;

	public GetPaymentMethodResponse getPaymentMethod(long paymentMethodId) throws TaboorQMSServiceException, Exception;

	public GetUserBankCardResponse getUserBankCard(long userbankCardId) throws TaboorQMSServiceException, Exception;

	public GenStatusResponse addUserBankCard(@Valid AddUserBankCardPayload addUserBankCardPayload)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse updateUserBankCard(@Valid AddUserBankCardPayload addUserBankCardPayload)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse deleteUserBankCard(long userBankCardId) throws TaboorQMSServiceException, Exception;

	public GenStatusResponse deleteUserById(long userId) throws TaboorQMSServiceException, Exception;

	public boolean verfiyEmailAddress(String encodedPayload) throws TaboorQMSServiceException, Exception;

	public GenStatusResponse addUserManual(AddUserManualPayload addUserManualPayload,
			@Valid MultipartFile userManualFile) throws TaboorQMSServiceException, Exception;

	public GetUserManualListResponse getAllUserManual() throws TaboorQMSServiceException, Exception;

	public GenStatusResponse deleteUserManual(long userManaulId) throws TaboorQMSServiceException, Exception;

	public GenStatusResponse deleteUserFAQ(long userFAQId) throws TaboorQMSServiceException, Exception;

	public RegisterUserResponse updateUser(RegisterUserPayload registerUserPayload, @Valid MultipartFile profileImage)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse forgetUserPassword(@Valid ResetPasswordPayload resetPasswordPayload)
			throws TaboorQMSServiceException, Exception;

    public GenStatusResponse updateProfilePicture(MultipartFile profileImage) throws TaboorQMSServiceException, Exception;

}
