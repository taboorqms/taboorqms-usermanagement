package com.taboor.qms.user.management.service;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.UserRole;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetUserRoleListResponse;
import com.taboor.qms.user.management.payload.AddUserRolePayload;
import com.taboor.qms.user.management.response.GetUserPrivilegeListResponse;

@Service
public interface UserRoleService {

	public UserRole getByRoleName(@Valid String roleName) throws TaboorQMSServiceException;

	public GetUserRoleListResponse getByRoleNameRest(@Valid String roleName) throws TaboorQMSServiceException;

	public GetUserRoleListResponse getAll()
			throws TaboorQMSServiceException, JsonGenerationException, JsonMappingException, IOException;

	public GenStatusResponse addUserRole(AddUserRolePayload addUserRolePayload) throws TaboorQMSServiceException;

	public GenStatusResponse deleteUserRole(GetByIdPayload getByIdPayload) throws TaboorQMSServiceException;

	public GetUserPrivilegeListResponse getPrivileges()
			throws TaboorQMSServiceException, JsonGenerationException, JsonMappingException, IOException;

	public GenStatusResponse updateUserRole(AddUserRolePayload addUserRolePayload) throws TaboorQMSServiceException;

}
