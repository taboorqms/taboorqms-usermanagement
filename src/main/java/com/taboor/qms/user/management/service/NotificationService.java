package com.taboor.qms.user.management.service;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetUserNotificationPayload;
import com.taboor.qms.core.payload.LoginUserPayload;
import com.taboor.qms.core.payload.PushUserNotificationPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAllUserNotificationsResponse;
import com.taboor.qms.core.response.GetIdListResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@Service
public interface NotificationService {

	public GenStatusResponse pushUserNotication(PushUserNotificationPayload pushUserNotificationPayload)
			throws FirebaseMessagingException, TaboorQMSServiceException;

	public GetValuesResposne getUserUnreadCount() throws TaboorQMSServiceException;

	public GetAllUserNotificationsResponse getUserNotications(GetUserNotificationPayload getUserNotificationPayload)
			throws Throwable;

	public GetIdListResponse updateNotificationReadStatus(
			@Valid GetByIdListPayload updateUserNotificationReadStatusPayload)
			throws TaboorQMSServiceException, JsonGenerationException, JsonMappingException, IOException;

	public GenStatusResponse testUserNotication(LoginUserPayload loginUserPayload)
			throws FirebaseMessagingException, TaboorQMSServiceException;

	public GetAllUserNotificationsResponse getUserEmailNotications(
			GetUserNotificationPayload getUserNotificationPayload) throws FirebaseMessagingException,
			TaboorQMSServiceException, JsonGenerationException, JsonMappingException, RestClientException, IOException;
}
