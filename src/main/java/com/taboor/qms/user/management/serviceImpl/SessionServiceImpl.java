package com.taboor.qms.user.management.serviceImpl;

import java.time.OffsetDateTime;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.taboor.qms.core.auth.JwtTokenProvider;
import com.taboor.qms.core.auth.MyUserDetailsService;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.payload.CreateSessionPayload;
import com.taboor.qms.core.response.CreateSessionResponse;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.user.management.service.SessionService;

import net.bytebuddy.utility.RandomString;

@Service
public class SessionServiceImpl implements SessionService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Autowired
	private MyUserDetailsService userDetailsService;

	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public CreateSessionResponse createUserSession(@Valid CreateSessionPayload createSessionPayload) throws Exception {

		Session session = new Session();
		session.setUser(createSessionPayload.getUser());

		session = saveSession(session);

		final UserDetails userDetails = userDetailsService.getUserDetailsObject(createSessionPayload.getUser());
		session.setSessionToken(jwtTokenProvider.generateToken(userDetails, session.getSessionId()));
		session.setRefreshToken(RandomString.make(40));
		session.setDeviceToken(createSessionPayload.getDeviceToken());
		session.setIpAddress(createSessionPayload.getIpAddress());
		session.setLoginTime(OffsetDateTime.now());
		session.setLogoutTime(null);

		session = saveSession(session);

		return new CreateSessionResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_SESSION_CREATED.getMessage(), session.getSessionToken(),
				session.getRefreshToken());
	}

	private Session saveSession(Session session) throws RestClientException, TaboorQMSServiceException {
		return RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/sessions/save",
				new HttpEntity<>(session, HttpHeadersUtils.getApplicationJsonHeader()), Session.class,
				xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
				xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());
	}

	@Override
	public Session getBySessionToken(String sessionToken) throws TaboorQMSServiceException {
		restTemplate = new RestTemplate();
		String uri = dbConnectorMicroserviceURL + "/tab/sessions/getActiveSession/sessionToken?sessionToken="
				+ sessionToken;
		ResponseEntity<Session> getSessionResponse = restTemplate.getForEntity(uri, Session.class);

		if (!getSessionResponse.getStatusCode().equals(HttpStatus.OK) || getSessionResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.SESSION_TOKEN_INVALID.getErrorCode() + "::"
					+ xyzErrorMessage.SESSION_TOKEN_INVALID.getErrorDetails());
		return getSessionResponse.getBody();
	}

	@Override
	public Session getByRefreshToken(String refreshToken) throws TaboorQMSServiceException, Exception {
		restTemplate = new RestTemplate();
		String uri = dbConnectorMicroserviceURL + "/tab/sessions/getByRefreshToken?refreshToken=" + refreshToken;
		ResponseEntity<Session> getSessionResponse = restTemplate.getForEntity(uri, Session.class);

		if (!getSessionResponse.getStatusCode().equals(HttpStatus.OK) || getSessionResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.REFRESH_TOKEN_INVALID.getErrorCode() + "::"
					+ xyzErrorMessage.REFRESH_TOKEN_INVALID.getErrorDetails());
		return getSessionResponse.getBody();
	}

	@Override
	public CreateSessionResponse refreshSession(String refreshToken) throws TaboorQMSServiceException, Exception {

		Session session = getByRefreshToken(refreshToken);
		final UserDetails userDetails = userDetailsService.getUserDetailsObject(session.getUser());
		session.setSessionToken(jwtTokenProvider.generateToken(userDetails, null));
		session.setRefreshToken(RandomString.make(40));
		session.setLoginTime(OffsetDateTime.now());
		session.setLogoutTime(null);

		String uri = dbConnectorMicroserviceURL + "/tab/sessions/save";

		HttpEntity<Session> entity = new HttpEntity<>(session, HttpHeadersUtils.getApplicationJsonHeader());

		ResponseEntity<Session> updateSessionResponse = restTemplate.postForEntity(uri, entity, Session.class);

		if (!updateSessionResponse.getStatusCode().equals(HttpStatus.OK) || updateSessionResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode() + "::"
					+ xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());

		return new CreateSessionResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_SESSION_REFRESHED.getMessage(),
				updateSessionResponse.getBody().getSessionToken(), updateSessionResponse.getBody().getRefreshToken());
	}

}
