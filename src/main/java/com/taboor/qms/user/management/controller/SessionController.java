package com.taboor.qms.user.management.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.payload.CreateSessionPayload;
import com.taboor.qms.core.response.CreateSessionResponse;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.LoginUserResponse;
import com.taboor.qms.user.management.service.SessionService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/session")
@CrossOrigin(origins = "*")
public class SessionController {

	private static final Logger logger = LoggerFactory.getLogger(SessionController.class);

	@Autowired
	SessionService sessionService;

	@ApiOperation(value = "Create User Session in the system.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User Session created Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody CreateSessionResponse createUserSession(
			@RequestBody @Valid final CreateSessionPayload createSessionPayload) throws Exception, Throwable {
		logger.info("Calling Create User Session - API");
		return sessionService.createUserSession(createSessionPayload);
	}

	@ApiOperation(value = "Refresh Session Token.", response = LoginUserResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Session refereshed Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/refresh", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GenStatusResponse refershSession(@RequestBody String refreshToken)
			throws Exception, Throwable {
		logger.info("Calling Refresh Session - API");
		return sessionService.refreshSession(refreshToken);
	}

}
