package com.taboor.qms.user.management.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetUserRoleListResponse;
import com.taboor.qms.user.management.payload.AddUserRolePayload;
import com.taboor.qms.user.management.response.GetUserPrivilegeListResponse;
import com.taboor.qms.user.management.service.UserRoleService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/userRole")
@CrossOrigin(origins = "*")
public class UserRoleController {

	private static final Logger logger = LoggerFactory.getLogger(UserRoleController.class);

	@Autowired
	UserRoleService userRoleService;

	@ApiOperation(value = "Get User Role By Role Name.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Used fetched Successfully."),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/get/rolename", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetUserRoleListResponse getByRoleName(@RequestBody String roleName) throws Exception {
		logger.info("Calling Get User Role By Role Name - API");
		return userRoleService.getByRoleNameRest(roleName);
	}

	@GetMapping("/get/all")
	public @ResponseBody GetUserRoleListResponse getAll() throws Exception, Throwable {
		logger.info("Calling Get All User Roles - API");
		return userRoleService.getAll();
	}

	@GetMapping("/getPrivileges")
	public @ResponseBody GetUserPrivilegeListResponse getAllPrivileges() throws Exception, Throwable {
		logger.info("Calling Get User Privilege - API");
		return userRoleService.getPrivileges();
	}

	@PostMapping("/add")
	public @ResponseBody GenStatusResponse addUserRole(@RequestBody AddUserRolePayload addUserRolePayload)
			throws Exception, Throwable {
		logger.info("Calling Add User Roles - API");
		return userRoleService.addUserRole(addUserRolePayload);
	}

	@PostMapping("/update")
	public @ResponseBody GenStatusResponse updateUserRole(@RequestBody AddUserRolePayload addUserRolePayload)
			throws Exception, Throwable {
		logger.info("Calling Update User Roles - API");
		return userRoleService.updateUserRole(addUserRolePayload);
	}

	@PostMapping("/delete")
	public @ResponseBody GenStatusResponse deleteUserRole(@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Delete User Roles - API");
		return userRoleService.deleteUserRole(getByIdPayload);
	}
}
