package com.taboor.qms.user.management.serviceImpl;

import java.io.UnsupportedEncodingException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.core.auth.JwtTokenProvider;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Customer;
import com.taboor.qms.core.model.PaymentMethod;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserBankCard;
import com.taboor.qms.core.model.UserFAQ;
import com.taboor.qms.core.model.UserFAQType;
import com.taboor.qms.core.model.UserFeedback;
import com.taboor.qms.core.model.UserManual;
import com.taboor.qms.core.model.UserPrivilegeMapper;
import com.taboor.qms.core.model.UserRoleMapper;
import com.taboor.qms.core.model.UserVerification;
import com.taboor.qms.core.payload.AddUserBankCardPayload;
import com.taboor.qms.core.payload.AddUserFAQPayload;
import com.taboor.qms.core.payload.AddUserFAQTypePayload;
import com.taboor.qms.core.payload.AddUserFeedbackPayload;
import com.taboor.qms.core.payload.ChangePasswordPayload;
import com.taboor.qms.core.payload.CreateSessionPayload;
import com.taboor.qms.core.payload.EditProfilePayload;
import com.taboor.qms.core.payload.EditProfileVerificationPayload;
import com.taboor.qms.core.payload.LoginUserPayload;
import com.taboor.qms.core.payload.RegisterUserPayload;
import com.taboor.qms.core.payload.ResetPasswordPayload;
import com.taboor.qms.core.payload.SendEmailPayload;
import com.taboor.qms.core.payload.VerificationCodePayload;
import com.taboor.qms.core.payload.VerifyUserPayload;
import com.taboor.qms.core.response.CreateSessionResponse;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAllUserFAQResponse;
import com.taboor.qms.core.response.GetAllUserFAQTypeResponse;
import com.taboor.qms.core.response.GetPaymentMethodResponse;
import com.taboor.qms.core.response.GetUserBankCardResponse;
import com.taboor.qms.core.response.GetUserResponse;
import com.taboor.qms.core.response.LoginUserResponse;
import com.taboor.qms.core.response.RegisterUserResponse;
import com.taboor.qms.core.response.UserAndPrivilegesResponse;
import com.taboor.qms.core.utils.DeviceName;
import com.taboor.qms.core.utils.EmailTemplates;
import com.taboor.qms.core.utils.FileDataObject;
import com.taboor.qms.core.utils.FileDataService;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.PrivilegeName;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.RoleName;
import com.taboor.qms.core.utils.Signature;
import com.taboor.qms.core.utils.TaboorQMSConfigurations;
import com.taboor.qms.core.utils.UserVerificationStatus;
import com.taboor.qms.user.management.controller.UserController;
import com.taboor.qms.user.management.payload.AddUserManualPayload;
import com.taboor.qms.user.management.response.GetUserManualListResponse;
import com.taboor.qms.user.management.service.SessionService;
import com.taboor.qms.user.management.service.UserService;
//import com.twilio.Twilio;
//import com.twilio.rest.api.v2010.account.Message;
//import com.twilio.type.PhoneNumber;

import org.apache.http.client.utils.URIBuilder;

import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.bytebuddy.utility.RandomString;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private SessionService sessionService;

    @Autowired
    private FileDataService fileDataService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    private RestTemplate restTemplate = new RestTemplate();

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Value("${url.microservice.db.connector}")
    private String dbConnectorMicroserviceURL;

    @Value("${url.microservice.email.service}")
    private String emailServiceMicroserviceURL;

    @Override
    public boolean checkUserExists(String email) {
        String uri = dbConnectorMicroserviceURL + "/tab/users/checkUserByEmail?email=" + email;
        ResponseEntity<Boolean> checkUserByEmailResponse = restTemplate.getForEntity(uri, Boolean.class);
        return checkUserByEmailResponse.getBody();
    }

    @Override
    public boolean checkUserExistsPhone(String phoneNumber) {
        String uri = dbConnectorMicroserviceURL + "/tab/users/checkUserByPhone?phoneNumber=" + phoneNumber;
        ResponseEntity<Boolean> checkUserByPhoneResponse = restTemplate.getForEntity(uri, Boolean.class);
        return checkUserByPhoneResponse.getBody();
    }

    @Override
    public GenStatusResponse checkUserExistsRest(String email) throws TaboorQMSServiceException, Exception {

        if (checkUserExists(email) == true) {
            return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.USER_EXISTS.getMessage());
        }
        return new GenStatusResponse(xyzResponseCode.ERROR.getCode(), xyzResponseMessage.USER_NOT_EXISTS.getMessage());
    }

    @Override
    public User getUserByEmail(String email) throws UnsupportedEncodingException {
        String uri = dbConnectorMicroserviceURL + "/tab/users/get/email?email=" + email;
        System.out.println(uri);
        ResponseEntity<User> getUserByEmailResponse = restTemplate.getForEntity(uri, User.class);
        System.out.println(getUserByEmailResponse);
        return getUserByEmailResponse.getBody();
    }

    @Override
    public User getUserByPhoneNumber(String number) throws UnsupportedEncodingException {
        String uri = dbConnectorMicroserviceURL + "/tab/users/get/phone_number?phone_number=" + number;
        ResponseEntity<User> getUserByEmailResponse = restTemplate.getForEntity(uri, User.class);
        return getUserByEmailResponse.getBody();
    }

    public UserAndPrivilegesResponse getUserAndPrivilegesByEmail(String email)
            throws UnsupportedEncodingException, RestClientException, TaboorQMSServiceException {
        return RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/users/getUserAndPrivileges/email?email=" + email,
                UserAndPrivilegesResponse.class, xyzErrorMessage.USER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
    }

    @Override
    public GetUserResponse getUserByEmailRest(String email) throws TaboorQMSServiceException, Exception {

        User user = getUserByEmail(email);
        if (user != null) {
            user.setPassword(null);
            return new GetUserResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_FETCHED.getMessage(),
                    user, "");
        }
        return new GetUserResponse(xyzResponseCode.ERROR.getCode(), xyzResponseMessage.USER_NOT_EXISTS.getMessage(),
                user, "");
    }

    @Override
    public GenStatusResponse verifyUser(VerifyUserPayload verifyUserPayload)
            throws TaboorQMSServiceException, UnsupportedEncodingException {

        User user = getUserByEmail(verifyUserPayload.getEmail());
        if (user == null) {
            user = getUserByPhoneNumber(verifyUserPayload.getEmail());
        }
        System.out.println("hahaha2===" + user);
        if (user == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
        }

        System.out.println(verifyUserPayload.getVerificationCode());
        System.out.println(verifyUserPayload.getVerificationMode());

//		 Get UserVerification
        HttpEntity<VerifyUserPayload> getUserVerificationEntity = new HttpEntity<>(verifyUserPayload,
                HttpHeadersUtils.getApplicationJsonHeader());

        String uri = dbConnectorMicroserviceURL + "/tab/userverifications/getByUserEmailAndModeAndCode";
        System.out.println(uri);
        ResponseEntity<UserVerification> getUserVerificationResponse = restTemplate.postForEntity(uri,
                getUserVerificationEntity, UserVerification.class);

        if (!getUserVerificationResponse.getStatusCode().equals(HttpStatus.OK)
                || getUserVerificationResponse.getBody() == null) {
            String uri2 = dbConnectorMicroserviceURL + "/tab/userverifications/getByUserPhoneAndModeAndCode";
            System.out.println(uri2);
            getUserVerificationResponse = restTemplate.postForEntity(uri2,
                    getUserVerificationEntity, UserVerification.class);
            if (!getUserVerificationResponse.getStatusCode().equals(HttpStatus.OK)
                    || getUserVerificationResponse.getBody() == null) {
                throw new TaboorQMSServiceException(xyzErrorMessage.VERIFICATION_CODE_INVALID.getErrorCode() + "::"
                        + xyzErrorMessage.VERIFICATION_CODE_INVALID.getErrorDetails());
            }
        }

        UserVerification userVerification = getUserVerificationResponse.getBody();
        int compareDate = userVerification.getExpiryTime().compareTo(OffsetDateTime.now());

        if (compareDate < 0) {
            throw new TaboorQMSServiceException(xyzErrorMessage.VERIFICATION_CODE_EXPIRED.getErrorCode() + "::"
                    + xyzErrorMessage.VERIFICATION_CODE_EXPIRED.getErrorDetails());
        }

        userVerification.setVerifiedTime(OffsetDateTime.now());
        userVerification.setVerificationStatus(UserVerificationStatus.VERIFIED.getStatus());

        uri = dbConnectorMicroserviceURL + "/tab/userverifications/save";

        HttpEntity<UserVerification> updateUserVerificationEntity = new HttpEntity<>(userVerification,
                HttpHeadersUtils.getApplicationJsonHeader());

        ResponseEntity<UserVerification> updateUserVerificationResponse = restTemplate.postForEntity(uri,
                updateUserVerificationEntity, UserVerification.class);

        if (!updateUserVerificationResponse.getStatusCode().equals(HttpStatus.OK)
                || updateUserVerificationResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USERVERIFICATION_NOT_UPDATED.getErrorCode() + "::"
                    + xyzErrorMessage.USERVERIFICATION_NOT_UPDATED.getErrorDetails());
        }

        user.setPhoneVerified(true);
        user.setEmailVerified(true);
        uri = dbConnectorMicroserviceURL + "/tab/users/save";
        HttpEntity<User> updateUserEntity = new HttpEntity<>(user, HttpHeadersUtils.getApplicationJsonHeader());

        ResponseEntity<User> updateUserResponse = restTemplate.postForEntity(uri, updateUserEntity, User.class);

        if (!updateUserResponse.getStatusCode().equals(HttpStatus.OK) || updateUserResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_UPDATED.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_VERIFIED.getMessage());
    }

    public GenStatusResponse sendOTP(@Valid VerificationCodePayload verificationCodePayload)
            throws TaboorQMSServiceException, Exception {

        Random random = new Random();
        int code = random.ints(11111, (99998 + 1)).findFirst().getAsInt();
        User user = getUserByEmail(verificationCodePayload.getEmail());
        System.out.println("hahaha===" + user);
        if (user == null) {
            user = getUserByPhoneNumber(verificationCodePayload.getEmail());
        }
        System.out.println("hahaha2===" + user);
        if (user == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
        }
        String emailSent = verificationCodePayload.getEmail();
        System.out.println("hahaha3===" + emailSent);
        System.out.println("hahaha4===" + user.getPhoneNumber());
        System.out.println("hahaha4===" + user.getEmail());
        if (emailSent.equals(user.getPhoneNumber())) {

            System.out.println("HERE" + user.getPhoneNumber());
            try {
                URI uri = new URI("https://api.smsglobal.com/http-api.php");
                System.out.println("URI created");

//				Message message = new Message(, , );
//				HttpTransport httpTransport = new HttpTransport(, , "");
                URIBuilder builder = new URIBuilder();
                System.out.println("URI builder created");
                builder.setScheme(uri.getScheme());
                builder.setHost(uri.getHost());
                builder.setPath(uri.getPath());
                builder.addParameter("action", "sendsms");
                builder.addParameter("user", "pjv9b734");
                builder.addParameter("password", "zadQr4Mw");
                builder.addParameter("from", "Taboor");
                builder.addParameter("to", user.getPhoneNumber());
                builder.addParameter("text", "Welcome to Taboor QMS. Thanks for signing up. Your verification code is " + code);
//				System.out.println(httpTransport.buildUrl(message));
                System.out.println("Builder createdsdasf");
                System.out.println("Builder created" + builder.toString());
                HttpGet request = new HttpGet(builder.toString());
                CloseableHttpClient httpClient = HttpClients.createDefault();
                CloseableHttpResponse response = httpClient.execute(request);
                System.out.println(response);
//				Twilio.init("ACa91b84300a514599b5445af7fb1f0b2d", "00f68a95d02f6204025565c5b0c6b8c3");
//
//				Message message = Message.creator(new PhoneNumber("+971544447938"), // To
//						new PhoneNumber("+15052192144"), // From
//						"Welcome to Taboor QMS. Thanks for signing up. Your verification code is " + code).create();

            } catch (Exception e) {
//				throw new TaboorQMSServiceException(xyzErrorMessage.USERVERIFICATION_OTP_NOT_SENT.getErrorCode() + "::"
//						+ xyzErrorMessage.USERVERIFICATION_OTP_NOT_SENT.getErrorDetails() + " - "
//						+ e.getLocalizedMessage().toString());
                return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                        xyzResponseMessage.USERVERIFICATION_OTP_SENT.getMessage());
            }
        } else {

//			Customer customer = RestUtil.getRequest(
//					dbConnectorMicroserviceURL + "/tab/customers/get/userId?userId=" + user.getUserId(), Customer.class,
//					xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorCode(),
//					xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorDetails());
            // Sending Password via Email
            String body = EmailTemplates.EMAIL_TEMPLATE;
            body = body.replaceAll("NAME", user.getName().toUpperCase());

//            logger.info("Guid: " + user.getGuid());
//            logger.info("Guid: " + user.getUserId());
            String encodedPayload = Base64.getEncoder()
                    .encodeToString(("userId=" + user.getUserId() + "&guid=" + user.getGuid()).getBytes());
//            logger.info("Guid: " + user.getGuid());
            String message = " '" + EmailTemplates.EMAIL_VERIFICATION_LINK + "?key=" + encodedPayload + "' ";
//            logger.info(message);
//            body = body.replaceAll("BODY", message);
            
            SendEmailPayload sendEmailPayload = new SendEmailPayload();
                sendEmailPayload.setBody(body);
                sendEmailPayload.setEmail(verificationCodePayload.getEmail());
                sendEmailPayload.setSubject("TaboorQMS User Verification Email");               
                RestUtil.postRequest(emailServiceMicroserviceURL + "/email/send",
                        new HttpEntity<>(sendEmailPayload, HttpHeadersUtils.getApplicationJsonHeader()), GenStatusResponse.class,
                        xyzErrorMessage.EMAIL_NOT_SENT.getErrorCode(), xyzErrorMessage.EMAIL_NOT_SENT.getErrorDetails());
//            EmailSending.sendEmail(verificationCodePayload.getEmail(), body, "TaboorQMS User Verification Email");
        }

        UserVerification userVerification = new UserVerification();
        userVerification.setCreatedTime(OffsetDateTime.now());
        userVerification.setExpiryTime(OffsetDateTime.now().plusMinutes(10));
        userVerification.setUser(user);
        userVerification.setVerificationCode(Integer.toString(code));
        userVerification.setVerificationMode(verificationCodePayload.getVerificationMode());
        userVerification.setVerificationStatus(UserVerificationStatus.NOTVERIFIED.getStatus());
        userVerification.setVerifiedTime(OffsetDateTime.now());
        if (emailSent.equals(user.getPhoneNumber())) {
            userVerification.setVerificationMedium(user.getPhoneNumber());
        } else {
            userVerification.setVerificationMedium(user.getEmail());
        }

        String uri = dbConnectorMicroserviceURL + "/tab/userverifications/save";

        HttpEntity<UserVerification> entity = new HttpEntity<>(userVerification,
                HttpHeadersUtils.getApplicationJsonHeader());

        ResponseEntity<UserVerification> createUserResponse = restTemplate.postForEntity(uri, entity,
                UserVerification.class);

        if (!createUserResponse.getStatusCode().equals(HttpStatus.OK) || createUserResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USERVERIFICATION_NOT_CREATED.getErrorCode() + "::"
                    + xyzErrorMessage.USERVERIFICATION_NOT_CREATED.getErrorDetails());
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USERVERIFICATION_OTP_SENT.getMessage());
    }

    @Override
    public RegisterUserResponse registerUser(@Valid RegisterUserPayload registerUserPayload, MultipartFile profileImage)
            throws TaboorQMSServiceException, Exception {

        if (checkUserExists(registerUserPayload.getEmail()) == true) {
            throw new TaboorQMSServiceException(
                    xyzErrorMessage.USER_EXISTS.getErrorCode() + "::" + xyzErrorMessage.USER_EXISTS.getErrorDetails());
        }
        if (checkUserExistsPhone(registerUserPayload.getPhoneNumber()) == true) {
            throw new TaboorQMSServiceException(
                    xyzErrorMessage.USER_EXISTS_PHONE.getErrorCode() + "::" + xyzErrorMessage.USER_EXISTS_PHONE.getErrorDetails());
        }

        User user = new User();
        user.setEmail(registerUserPayload.getEmail());
        user.setName(registerUserPayload.getName());
        user.setPassword(Signature.encrypt(registerUserPayload.getPassword(),
                TaboorQMSConfigurations.PASSWORD_ENCRYPTION_KEY.getValue()));
        user.setPhoneNumber(registerUserPayload.getPhoneNumber());
        user.setCreatedTime(OffsetDateTime.now());
        user.setGuid(RandomString.make(64));
        user.setEmailVerified(false);
        user.setPhoneVerified(false);

        user.setProfileImageUrl("");
        if (profileImage != null) {
            if (!profileImage.isEmpty()) {
                if (!Arrays.asList("image/jpeg", "image/jpg", "image/png").contains(profileImage.getContentType())) {
                    throw new TaboorQMSServiceException(xyzErrorMessage.IMAGE_FORMAT_WRONG.getErrorCode() + "::"
                            + xyzErrorMessage.IMAGE_FORMAT_WRONG.getErrorDetails());
                }

                String filePath = TaboorQMSConfigurations.HOST_USER_IMAGE_FOLDER.getValue()
                        .concat(String.valueOf(OffsetDateTime.now().toEpochSecond()).concat(RandomString.make(10)))
                        .concat(profileImage.getOriginalFilename()
                                .substring(profileImage.getOriginalFilename().indexOf(".")));
                Boolean saved = fileDataService.save(Arrays.asList(new FileDataObject(profileImage, filePath)));
                if (saved) {
                    user.setProfileImageUrl(Base64.getEncoder().encodeToString(filePath.getBytes()));
                } else {
                    throw new TaboorQMSServiceException(xyzErrorMessage.FILE_NOT_SAVED.getErrorCode() + "::"
                            + xyzErrorMessage.FILE_NOT_SAVED.getErrorDetails());
                }
            }
        }
        System.out.println(registerUserPayload);
        user.setRoleName(registerUserPayload.getUserRole().getRoleName());

        String uri = dbConnectorMicroserviceURL + "/tab/users/save";
        HttpEntity<User> userEntity = new HttpEntity<>(user, HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<User> createUserResponse = restTemplate.postForEntity(uri, userEntity, User.class);

        if (!createUserResponse.getStatusCode().equals(HttpStatus.OK) || createUserResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_CREATED.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_CREATED.getErrorDetails());
        }

        UserRoleMapper userRoleMapper = new UserRoleMapper();
        userRoleMapper.setUser(createUserResponse.getBody());
        userRoleMapper.setUserRole(registerUserPayload.getUserRole());

        uri = dbConnectorMicroserviceURL + "/tab/userrolemapper/save";
        HttpEntity<UserRoleMapper> userRoleMapperEntity = new HttpEntity<>(userRoleMapper,
                HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<UserRoleMapper> createUserRoleMapperResponse = restTemplate.postForEntity(uri,
                userRoleMapperEntity, UserRoleMapper.class);

        if (registerUserPayload.getAssignRolePrivileges()) {
            List<UserPrivilegeMapper> userPrivilegeMappers = new ArrayList<UserPrivilegeMapper>();

            registerUserPayload.getUserRole().getUserPrivileges().forEach(entity -> {
                UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
                userPrivilegeMapper.setUser(createUserResponse.getBody());
                userPrivilegeMapper.setUserPrivilege(entity);
                userPrivilegeMappers.add(userPrivilegeMapper);
            });

            uri = dbConnectorMicroserviceURL + "/tab/userprivilegemapper/saveAll";
            HttpEntity<?> httpEntity = new HttpEntity<>(userPrivilegeMappers,
                    HttpHeadersUtils.getApplicationJsonHeader());

            System.out.println(uri);
            System.out.println(httpEntity);
            System.out.println(Boolean.class);
            ResponseEntity<Boolean> createUserPrivilegeMappersResponse = restTemplate.postForEntity(uri, httpEntity,
                    Boolean.class);
            System.out.println("HERE");

            if (!createUserPrivilegeMappersResponse.getStatusCode().equals(HttpStatus.OK)
                    || createUserPrivilegeMappersResponse.getBody().equals(false)) {
                throw new TaboorQMSServiceException(xyzErrorMessage.USERPRIVILEGEMAPPER_NOT_CREATED.getErrorCode()
                        + "::" + xyzErrorMessage.USERPRIVILEGEMAPPER_NOT_CREATED.getErrorDetails());
            }
        }

        // Sending Email
        String body = EmailTemplates.EMAIL_TEMPLATE;
        body = body.replaceAll("::BODY::", "Your account password is : " + registerUserPayload.getPassword());
        body = body.replaceAll("NAME", user.getName().toUpperCase());

        String encodedPayload = Base64.getEncoder()
                .encodeToString(("userId=" + user.getUserId() + "&guid=" + user.getGuid()).getBytes());
//        logger.info("Guid: " + user.getGuid());
        String message = " '" + EmailTemplates.EMAIL_VERIFICATION_LINK + "?key=" + encodedPayload + "' ";
//        logger.info(message);
        body = body.replaceAll("BODY", message);

        SendEmailPayload sendEmailPayload = new SendEmailPayload();
            sendEmailPayload.setBody(body);
            sendEmailPayload.setEmail(user.getEmail());
            sendEmailPayload.setSubject("TaboorQMS Customer Verification Email");   
            RestUtil.postRequest(emailServiceMicroserviceURL + "/email/send",
                    new HttpEntity<>(sendEmailPayload, HttpHeadersUtils.getApplicationJsonHeader()), GenStatusResponse.class,
                    xyzErrorMessage.EMAIL_NOT_SENT.getErrorCode(), xyzErrorMessage.EMAIL_NOT_SENT.getErrorDetails());

        return new RegisterUserResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_REGISTERED.getMessage(), createUserResponse.getBody());
    }

    @Override
    public GenStatusResponse sendOTPUpdated(@Valid EditProfilePayload editPayload)
            throws TaboorQMSServiceException, Exception {

        Random random = new Random();
        int code = random.ints(11111, (99998 + 1)).findFirst().getAsInt();
        int mode = 0;
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        if (session.getUser() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
        }

        if (editPayload.getEmail() != null && !editPayload.getEmail().isEmpty()) {
            mode = 1;
//			EmailSending.sendEmail(editPayload.getEmail(), code);
        }

        UserVerification userVerification = new UserVerification();
        userVerification.setCreatedTime(OffsetDateTime.now());
        userVerification.setExpiryTime(OffsetDateTime.now().plusMinutes(10));
        userVerification.setUser(session.getUser());
        userVerification.setVerificationCode(Integer.toString(code));
        userVerification.setVerificationMode(mode);
        userVerification.setVerificationStatus(UserVerificationStatus.NOTVERIFIED.getStatus());
        userVerification.setVerifiedTime(OffsetDateTime.now());
        if (mode == 1) {
            userVerification.setVerificationMedium(editPayload.getEmail());
        }

        HttpEntity<UserVerification> entity = new HttpEntity<>(userVerification,
                HttpHeadersUtils.getApplicationJsonHeader());

        RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/userverifications/save", entity, UserVerification.class,
                xyzErrorMessage.USERVERIFICATION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.USERVERIFICATION_NOT_CREATED.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USERVERIFICATION_OTP_SENT.getMessage());

    }

    @Override
    public GenStatusResponse verifyUserUpdated(EditProfileVerificationPayload verifyUserPayload)
            throws TaboorQMSServiceException {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        if (session.getUser() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
        }

//		VerifyUserPayload payload = new VerifyUserPayload();
//		payload.setEmail(session.getUser().getEmail());
//		payload.setVerificationCode(verifyUserPayload.getVerificationCode());
//		payload.setVerificationMode(verifyUserPayload.getVerificationMode());
//
//		// Get UserVerification
//		HttpEntity<VerifyUserPayload> getUserVerificationEntity = new HttpEntity<>(payload,
//				HttpHeadersUtils.getApplicationJsonHeader());
//
//		String uri = dbConnectorMicroserviceURL + "/tab/userverifications/getByUserEmailAndModeAndCode";
//
//		ResponseEntity<UserVerification> getUserVerificationResponse = restTemplate.postForEntity(uri,
//				getUserVerificationEntity, UserVerification.class);
//
//		if (!getUserVerificationResponse.getStatusCode().equals(HttpStatus.OK)
//				|| getUserVerificationResponse.getBody() == null)
//			throw new TaboorQMSServiceException(xyzErrorMessage.VERIFICATION_CODE_INVALID.getErrorCode() + "::"
//					+ xyzErrorMessage.VERIFICATION_CODE_INVALID.getErrorDetails());
//
//		UserVerification userVerification = getUserVerificationResponse.getBody();
//
//		if (userVerification.getVerificationMode() == 0) {
//			int compareDate = userVerification.getExpiryTime().compareTo(OffsetDateTime.now());
//
//			if (compareDate < 0) {
//				throw new TaboorQMSServiceException(xyzErrorMessage.VERIFICATION_CODE_EXPIRED.getErrorCode() + "::"
//						+ xyzErrorMessage.VERIFICATION_CODE_EXPIRED.getErrorDetails());
//			}
//		}
//
//		userVerification.setVerifiedTime(OffsetDateTime.now());
//		userVerification.setVerificationStatus(UserVerificationStatus.VERIFIED.getStatus());
//
//		uri = dbConnectorMicroserviceURL + "/tab/userverifications/save";
//
//		HttpEntity<UserVerification> updateUserVerificationEntity = new HttpEntity<>(userVerification,
//				HttpHeadersUtils.getApplicationJsonHeader());
//
//		ResponseEntity<UserVerification> updateUserVerificationResponse = restTemplate.postForEntity(uri,
//				updateUserVerificationEntity, UserVerification.class);
//
//		if (!updateUserVerificationResponse.getStatusCode().equals(HttpStatus.OK)
//				|| updateUserVerificationResponse.getBody() == null)
//			throw new TaboorQMSServiceException(xyzErrorMessage.USERVERIFICATION_NOT_UPDATED.getErrorCode() + "::"
//					+ xyzErrorMessage.USERVERIFICATION_NOT_UPDATED.getErrorDetails());
        session.getUser().setEmailVerified(true);
        session.getUser().setPhoneVerified(true);

        HttpEntity<User> updateUserEntity = new HttpEntity<>(session.getUser(),
                HttpHeadersUtils.getApplicationJsonHeader());

        ResponseEntity<User> updateUserResponse = restTemplate
                .postForEntity(dbConnectorMicroserviceURL + "/tab/users/save", updateUserEntity, User.class);

        if (!updateUserResponse.getStatusCode().equals(HttpStatus.OK) || updateUserResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_UPDATED.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_VERIFIED.getMessage());

    }

    @Override
    public GenStatusResponse changeUserPassword(@Valid ChangePasswordPayload changePasswordPayload)
            throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        if (session.getUser() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
        }

        if (!Signature
                .decrypt(session.getUser().getPassword(), TaboorQMSConfigurations.PASSWORD_ENCRYPTION_KEY.getValue())
                .equals(changePasswordPayload.getOldPassword())) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_PASSWORD_INCORRECT.getErrorCode() + "::"
                    + xyzErrorMessage.USER_PASSWORD_INCORRECT.getErrorDetails());
        }

        session.getUser().setPassword(Signature.encrypt(changePasswordPayload.getNewPassword(),
                TaboorQMSConfigurations.PASSWORD_ENCRYPTION_KEY.getValue()));

        String uri = dbConnectorMicroserviceURL + "/tab/users/save";
        HttpEntity<User> updateUserEntity = new HttpEntity<>(session.getUser(),
                HttpHeadersUtils.getApplicationJsonHeader());

        ResponseEntity<User> updateUserResponse = restTemplate.postForEntity(uri, updateUserEntity, User.class);

        if (!updateUserResponse.getStatusCode().equals(HttpStatus.OK) || updateUserResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_UPDATED.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_PASSWORD_CHANGED.getMessage());
    }

    @Override
    public GenStatusResponse resetUserPassword(ResetPasswordPayload resetPasswordPayload)
            throws TaboorQMSServiceException, Exception {

        User user = getUserByEmail(resetPasswordPayload.getEmail());
        if (user == null) {
            user = getUserByPhoneNumber(resetPasswordPayload.getEmail());
        }
        System.out.println("hahaha2===" + user);
        if (user == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
        }

        user.setPassword(Signature.encrypt(resetPasswordPayload.getResetPassword(),
                TaboorQMSConfigurations.PASSWORD_ENCRYPTION_KEY.getValue()));

        String uri = dbConnectorMicroserviceURL + "/tab/users/save";
        HttpEntity<User> updateUserEntity = new HttpEntity<>(user, HttpHeadersUtils.getApplicationJsonHeader());

        ResponseEntity<User> updateUserResponse = restTemplate.postForEntity(uri, updateUserEntity, User.class);

        if (!updateUserResponse.getStatusCode().equals(HttpStatus.OK) || updateUserResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_UPDATED.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_PASSWORD_RESET.getMessage());
    }

    @Override
    public GenStatusResponse logoutUser() throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        session.setLogoutTime(OffsetDateTime.now());

        String uri = dbConnectorMicroserviceURL + "/tab/sessions/save";

        HttpEntity<Session> entity = new HttpEntity<>(session, HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<Session> updateSessionResponse = restTemplate.postForEntity(uri, entity, Session.class);
        if (!updateSessionResponse.getStatusCode().equals(HttpStatus.OK) || updateSessionResponse.getBody() == null) {
            throw new TaboorQMSServiceException(444 + "::" + "User not logout Successfully.");
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_LOGOUT.getMessage());
    }

    @Override
    public GenStatusResponse deleteUser() throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        String uri = dbConnectorMicroserviceURL + "/tab/users/delete?userId=" + session.getUser().getUserId();

        ResponseEntity<Integer> deleteUserResponse = restTemplate.getForEntity(uri, int.class);

        if (!deleteUserResponse.getStatusCode().equals(HttpStatus.OK) || deleteUserResponse.getBody().equals(0)) {
            return new GenStatusResponse(xyzResponseCode.ERROR.getCode(),
                    xyzResponseMessage.USER_NOT_DELETED.getMessage());
        }
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_DELETED.getMessage());
    }

    @Override
    public GenStatusResponse addUserFeedback(@Valid AddUserFeedbackPayload addUserFeedbackPayload)
            throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        UserFeedback userFeedback = new UserFeedback();
        userFeedback.setComment(addUserFeedbackPayload.getComment());
        userFeedback.setUser(session.getUser());

        String uri = dbConnectorMicroserviceURL + "/tab/userfeedbacks/save";
        HttpEntity<UserFeedback> createUserFeedbackEntity = new HttpEntity<>(userFeedback,
                HttpHeadersUtils.getApplicationJsonHeader());

        ResponseEntity<UserFeedback> createUserFeedbackResponse = restTemplate.postForEntity(uri,
                createUserFeedbackEntity, UserFeedback.class);

        if (!createUserFeedbackResponse.getStatusCode().equals(HttpStatus.OK)
                || createUserFeedbackResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USERFEEDBACK_NOT_CREATED.getErrorCode() + "::"
                    + xyzErrorMessage.USERFEEDBACK_NOT_CREATED.getErrorDetails());
        }
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_FEEDBACK_ADDED.getMessage());
    }

    @Override
    public GenStatusResponse addUserFAQ(@Valid AddUserFAQPayload addFAQPayload)
            throws TaboorQMSServiceException, Exception {

        UserFAQ userFAQ = new UserFAQ();
        if (addFAQPayload.getUserFAQId() != null) {
            if (addFAQPayload.getUserFAQId() > 0) {
                userFAQ = RestUtil.getRequest(
                        dbConnectorMicroserviceURL + "/tab/userfaqs/get/id?userFAQId=" + addFAQPayload.getUserFAQId(),
                        UserFAQ.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                        xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
            }
        }
        userFAQ.setQuestion(addFAQPayload.getQuestion());
        userFAQ.setAnswer(addFAQPayload.getAnswer());

        if (userFAQ.getUserFAQType() == null) {
            userFAQ.setUserFAQType(RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/userfaqtypes/get/id?userFAQTypeId="
                    + addFAQPayload.getFaqTypeId(),
                    UserFAQType.class, xyzErrorMessage.USERFAQ_TYPE_INVALID.getErrorCode(),
                    xyzErrorMessage.USERFAQ_TYPE_INVALID.getErrorDetails()));
        }

        RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/userfaqs/save",
                new HttpEntity<>(userFAQ, HttpHeadersUtils.getApplicationJsonHeader()), UserFAQ.class,
                xyzErrorMessage.USERFAQ_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.USERFAQ_NOT_CREATED.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_FAQ_ADDED.getMessage());
    }

    @Override
    public GetAllUserFAQResponse getAllUserFAQs() throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/userfaqs/get/all";
        ResponseEntity<?> getAllUserFAQsResponse = restTemplate.getForEntity(uri, List.class);

        List<UserFAQ> userFAQList = GenericMapper.convertListToObject(getAllUserFAQsResponse.getBody(),
                new TypeReference<List<UserFAQ>>() {
        });

        if (userFAQList == null) {
            userFAQList = new ArrayList<UserFAQ>();
        }
        return new GetAllUserFAQResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_FAQ_FETCHED.getMessage(), userFAQList);
    }

    @Override
    public GenStatusResponse addUserFAQType(@Valid AddUserFAQTypePayload addUserFAQTypePayload)
            throws TaboorQMSServiceException, Exception {

        UserFAQType userFAQType = new UserFAQType();
        userFAQType.setUserFaqTypeName(addUserFAQTypePayload.getUserFAQTypeName());

        String uri = dbConnectorMicroserviceURL + "/tab/userfaqtypes/save";
        HttpEntity<UserFAQType> createUserFAQTypeEntity = new HttpEntity<>(userFAQType,
                HttpHeadersUtils.getApplicationJsonHeader());

        ResponseEntity<UserFAQType> createUserFAQTypeResponse = restTemplate.postForEntity(uri, createUserFAQTypeEntity,
                UserFAQType.class);

        if (!createUserFAQTypeResponse.getStatusCode().equals(HttpStatus.OK)
                || createUserFAQTypeResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USERFAQTYPE_NOT_CREATED.getErrorCode() + "::"
                    + xyzErrorMessage.USERFAQTYPE_NOT_CREATED.getErrorDetails());
        }
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_FAQ_TYPE_ADDED.getMessage());
    }

    @Override
    public GetAllUserFAQTypeResponse getAllUserFAQTypes() throws TaboorQMSServiceException, Exception {

        String uri = dbConnectorMicroserviceURL + "/tab/userfaqtypes/get/all";
        ResponseEntity<?> getAllUserFAQTypesResponse = restTemplate.getForEntity(uri, List.class);

        List<UserFAQType> userFAQTypeList = GenericMapper.convertListToObject(getAllUserFAQTypesResponse.getBody(),
                new TypeReference<List<UserFAQType>>() {
        });

        return new GetAllUserFAQTypeResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_FAQ_TYPE_FETCHED.getMessage(), userFAQTypeList);
    }

    @Override
    public LoginUserResponse loginUser(@Valid LoginUserPayload loginUserPayload)
            throws TaboorQMSServiceException, Exception {
        System.out.println(loginUserPayload.getEmail());
        System.out.println(loginUserPayload.getPassword());
        System.out.println(loginUserPayload.getDeviceToken());
        System.out.println(loginUserPayload.getDeviceCode());
        String email = loginUserPayload.getEmail();
        String password = loginUserPayload.getPassword();
        String deviceToken = loginUserPayload.getDeviceToken();
        Integer deviceCode = loginUserPayload.getDeviceCode();

        UserAndPrivilegesResponse userAndPrivilegesResponse = getUserAndPrivilegesByEmail(email);
        User user = userAndPrivilegesResponse.getUser();

//        logger.info("ای میل: " + user.getEmail() + " ,, " + "پاس ورڈ: "
//                + Signature.decrypt(user.getPassword(), TaboorQMSConfigurations.PASSWORD_ENCRYPTION_KEY.getValue()));
        if (!Signature.decrypt(user.getPassword(), TaboorQMSConfigurations.PASSWORD_ENCRYPTION_KEY.getValue())
                .equals(password)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_PASSWORD_INCORRECT.getErrorCode() + "::"
                    + xyzErrorMessage.USER_PASSWORD_INCORRECT.getErrorDetails());
        }

        if (deviceCode.equals(DeviceName.ADMIN_PANEL_APP.getCode())) {
            if (!userAndPrivilegesResponse.getPrivileges().contains(PrivilegeName.Admin_Panel_View_Only)) {
                throw new TaboorQMSServiceException(xyzErrorMessage.ACCESS_DENIED.getErrorCode() + "::"
                        + xyzErrorMessage.ACCESS_DENIED.getErrorDetails());
            }
        } else if (deviceCode.equals(DeviceName.CUSTOMER_APP.getCode())) {
            if (!user.getRoleName().equals(RoleName.Customer.name())) {
                throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_CUSTOMER_ACCESS.getErrorCode() + "::"
                        + xyzErrorMessage.USERROLE_CUSTOMER_ACCESS.getErrorDetails());
            }
            if (!user.getPhoneVerified()) {
                throw new TaboorQMSServiceException(xyzErrorMessage.USER_PHONE_NOT_VERIFIED.getErrorCode() + "::"
                        + xyzErrorMessage.USER_PHONE_NOT_VERIFIED.getErrorDetails());
            }
        } else if (deviceCode.equals(DeviceName.KIOSIK_APP.getCode())
                && !user.getRoleName().equals(RoleName.Branch_Admin.name())) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_KIOSIK_ACCESS.getErrorCode() + "::"
                    + xyzErrorMessage.USERROLE_KIOSIK_ACCESS.getErrorDetails());
        } else if (deviceCode.equals(DeviceName.COUNTER_APP.getCode())
                && !user.getRoleName().equals(RoleName.Branch_Admin.name())) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_COUNTER_ACCESS.getErrorCode() + "::"
                    + xyzErrorMessage.USERROLE_COUNTER_ACCESS.getErrorDetails());
        } else if (deviceCode.equals(DeviceName.AGENT_APP.getCode())
                && user.getRoleName().equals(RoleName.Branch_Admin.name())) {
            List<?> sessions = RestUtil.getRequestNoCheck(
                    dbConnectorMicroserviceURL + "/tab/sessions/getByUserEmail?email=" + user.getEmail(), List.class);

//			List<Session> sessionList = GenericMapper.convertListToObject(sessions,
//					new TypeReference<List<Session>>() {
//					});
            if (!sessions.isEmpty()) {
                throw new TaboorQMSServiceException(xyzErrorMessage.SESSION_ALREADY_EXISTS.getErrorCode() + "::"
                        + xyzErrorMessage.SESSION_ALREADY_EXISTS.getErrorDetails());
            }

        }

        // Create User Session
        CreateSessionPayload createSessionPayload = new CreateSessionPayload();
        createSessionPayload.setDeviceToken(deviceToken);
        createSessionPayload.setIpAddress(loginUserPayload.getIpAddress());
        createSessionPayload.setUser(user);
        CreateSessionResponse createSessionResponse = sessionService.createUserSession(createSessionPayload);

        return new LoginUserResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_LOGIN.getMessage(),
                createSessionResponse.getSessionToken(), createSessionResponse.getRefreshToken(), user.getRoleName(),
                userAndPrivilegesResponse.getPrivileges(),
                new Date(jwtTokenProvider.getExpirationFromJWT(createSessionResponse.getSessionToken())));
    }

    @Override
    public GetPaymentMethodResponse getAllPaymentMethods() throws TaboorQMSServiceException, Exception {

        String uri = dbConnectorMicroserviceURL + "/tab/paymentmethods/get/all";
        ResponseEntity<?> getAllPaymentMethodsResponse = restTemplate.getForEntity(uri, List.class);

        List<PaymentMethod> paymentMethodList = GenericMapper
                .convertListToObject(getAllPaymentMethodsResponse.getBody(), new TypeReference<List<PaymentMethod>>() {
                });

        return new GetPaymentMethodResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.PAYMENT_METHOD_FETCHED.getMessage(), paymentMethodList);
    }

    @Override
    public GetPaymentMethodResponse getPaymentMethod(long paymentMethodId) throws TaboorQMSServiceException, Exception {

        String uri = dbConnectorMicroserviceURL + "/tab/paymentmethods/get/id?paymentMethodId=" + paymentMethodId;
        List<PaymentMethod> getPaymentMethodResponse = new ArrayList<PaymentMethod>();
        try {

            ResponseEntity<PaymentMethod> paymentMethodResponseEntity = restTemplate.getForEntity(uri,
                    PaymentMethod.class);
            getPaymentMethodResponse.add(paymentMethodResponseEntity.getBody());
        } catch (Exception e) {
            throw new TaboorQMSServiceException(444 + "::" + e.getLocalizedMessage());
        }

        return new GetPaymentMethodResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.PAYMENT_METHOD_FETCHED.getMessage(), getPaymentMethodResponse);
    }

    @Override
    public GetUserBankCardResponse getUserBankCards() throws TaboorQMSServiceException, Exception {

        // Object userDetails =
        // SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        // UserDetails aa = (UserDetails) userDetails;
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        String uri = dbConnectorMicroserviceURL + "/tab/userbankcards/get/user?userId=" + session.getUser().getUserId();
        ResponseEntity<?> getAllUserBankCardsResponse = restTemplate.getForEntity(uri, List.class);

        List<UserBankCard> userBankCardList = GenericMapper.convertListToObject(getAllUserBankCardsResponse.getBody(),
                new TypeReference<List<UserBankCard>>() {
        });

        return new GetUserBankCardResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_BANK_CARD_FETCHED.getMessage(), userBankCardList);
    }

    @Override
    public GetUserBankCardResponse getUserBankCard(long userbankCardId) throws TaboorQMSServiceException, Exception {

        String uri = dbConnectorMicroserviceURL + "/tab/userbankcards/get/id?userBankCardId=" + userbankCardId;
        ResponseEntity<UserBankCard> getUserBankCardResponse = restTemplate.getForEntity(uri, UserBankCard.class);
        List<UserBankCard> userBankCardList = new ArrayList<UserBankCard>();
        userBankCardList.add(getUserBankCardResponse.getBody());
        return new GetUserBankCardResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_BANK_CARD_FETCHED.getMessage(), userBankCardList);
    }

    @Override
    public GenStatusResponse addUserBankCard(@Valid AddUserBankCardPayload addUserBankCardPayload)
            throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

//		Customer customer = RestUtil.getRequest(
//				dbConnectorMicroserviceURL + "/tab/customers/get/userId?userId=" + session.getUser().getUserId(),
//				Customer.class, xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorCode(),
//				xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorDetails());
        if (!session.getUser().getEmailVerified()) {
            throw new TaboorQMSServiceException(xyzErrorMessage.EMAIL_NOT_VERIFIED.getErrorCode() + "::"
                    + xyzErrorMessage.EMAIL_NOT_VERIFIED.getErrorDetails());
        }

        PaymentMethod getPaymentMethodResponse = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/paymentmethods/get/id?paymentMethodId="
                + addUserBankCardPayload.getPaymentMethodId(),
                PaymentMethod.class, xyzErrorMessage.PAYMENT_METHOD_INVALID.getErrorCode(),
                xyzErrorMessage.PAYMENT_METHOD_INVALID.getErrorDetails());

        String[] expiry = addUserBankCardPayload.getExpiryDate().split("/");
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
//        System.out.println(currentYear);
//        System.out.println(currentMonth);
//        System.out.println(Integer.parseInt(expiry[1]));
//        System.out.println(Integer.parseInt(expiry[0]));
        if (currentYear > Integer.parseInt(expiry[1]) || (currentYear == Integer.parseInt(expiry[1]) && currentMonth > Integer.parseInt(expiry[0]))) {
            throw new TaboorQMSServiceException(xyzErrorMessage.CARD_DATE_EXPIRED.getErrorCode() + "::"
                    + xyzErrorMessage.CARD_DATE_EXPIRED.getErrorDetails());
        }

        UserBankCard userBankCard = new UserBankCard();
        userBankCard.setCardTitle(addUserBankCardPayload.getCardTitle());
        userBankCard.setCardNumber(addUserBankCardPayload.getCardNumber());
        userBankCard.setCardType(addUserBankCardPayload.getCardType());

        userBankCard.setExpiryMonth(Integer.parseInt(expiry[0]));
        userBankCard.setExpiryYear(Integer.parseInt(expiry[1]));
        userBankCard.setCardCvv(addUserBankCardPayload.getCardCvv());
        userBankCard.setPaymentMethod(getPaymentMethodResponse);
        userBankCard.setBalance(2000.0);// TODO
        userBankCard.setUser(session.getUser());

        HttpEntity<UserBankCard> entity = new HttpEntity<>(userBankCard, HttpHeadersUtils.getApplicationJsonHeader());
        RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/userbankcards/save", entity, UserBankCard.class,
                xyzErrorMessage.USERBANKCARD_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.USERBANKCARD_NOT_CREATED.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_BANK_CARD_ADDED.getMessage());
    }

    @Override
    public GenStatusResponse updateUserBankCard(@Valid AddUserBankCardPayload addUserBankCardPayload)
            throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        UserBankCard getUserBankCardResponse = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/userbankcards/get/id?userBankCardId="
                + addUserBankCardPayload.getUserBankCardId(),
                UserBankCard.class, xyzErrorMessage.USER_BANK_CARD_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.USER_BANK_CARD_NOT_EXISTS.getErrorDetails());

        String[] expiry = addUserBankCardPayload.getExpiryDate().split("/");
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
        if (currentYear > Integer.parseInt(expiry[1]) || (currentYear == Integer.parseInt(expiry[1]) && currentMonth > Integer.parseInt(expiry[0]))) {
            throw new TaboorQMSServiceException(xyzErrorMessage.CARD_DATE_EXPIRED.getErrorCode() + "::"
                    + xyzErrorMessage.CARD_DATE_EXPIRED.getErrorDetails());
        }
        if (!getUserBankCardResponse.getCardTitle().equals(addUserBankCardPayload.getCardTitle())) {
            getUserBankCardResponse.setCardTitle(addUserBankCardPayload.getCardTitle());
        }

        if (!getUserBankCardResponse.getCardNumber().equals(addUserBankCardPayload.getCardNumber())) {
            getUserBankCardResponse.setCardNumber(addUserBankCardPayload.getCardNumber());
        }

        if (!getUserBankCardResponse.getCardType().equals(addUserBankCardPayload.getCardType())) {
            getUserBankCardResponse.setCardType(addUserBankCardPayload.getCardType());
        }

        if (getUserBankCardResponse.getExpiryMonth() != Integer.parseInt(expiry[0])) {
            getUserBankCardResponse.setExpiryMonth(Integer.parseInt(expiry[0]));
        }

        if (getUserBankCardResponse.getExpiryYear() != Integer.parseInt(expiry[1])) {
            getUserBankCardResponse.setExpiryYear(Integer.parseInt(expiry[1]));
        }

        if (!getUserBankCardResponse.getCardCvv().equals(addUserBankCardPayload.getCardCvv())) {
            getUserBankCardResponse.setCardCvv(addUserBankCardPayload.getCardCvv());
        }

        if (!getUserBankCardResponse.getPaymentMethod().getPaymentMethodId()
                .equals(addUserBankCardPayload.getPaymentMethodId())) {

            PaymentMethod getPaymentMethodResponse = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/paymentmethods/get/id?paymentMethodId="
                    + addUserBankCardPayload.getPaymentMethodId(),
                    PaymentMethod.class, xyzErrorMessage.PAYMENT_METHOD_INVALID.getErrorCode(),
                    xyzErrorMessage.PAYMENT_METHOD_INVALID.getErrorDetails());

            getUserBankCardResponse.setPaymentMethod(getPaymentMethodResponse);
        }

        getUserBankCardResponse.setUser(session.getUser());
        HttpEntity<UserBankCard> entity = new HttpEntity<>(getUserBankCardResponse,
                HttpHeadersUtils.getApplicationJsonHeader());

        RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/userbankcards/save", entity, UserBankCard.class,
                xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorCode(),
                xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_BANK_CARD_UPDATED.getMessage());
    }

    @Override
    public GenStatusResponse deleteUserBankCard(long userBankCardId) throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/userbankcards/delete?userBankCardId=" + userBankCardId;
        ResponseEntity<Integer> deleteUserBankCardResponse = restTemplate.getForEntity(uri, Integer.class);
        if (!deleteUserBankCardResponse.getStatusCode().equals(HttpStatus.OK)
                || deleteUserBankCardResponse.getBody().equals(0)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USERBANKCARD_NOT_DELETED.getErrorCode() + "::"
                    + xyzErrorMessage.USERBANKCARD_NOT_DELETED.getErrorDetails());
        }
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_BANK_CARD_DELETED.getMessage());
    }

    @Override
    public GetUserResponse getUserProfile() throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        Customer customer = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/customers/get/userId?userId="
                + myUserDetails.getSession().getUser().getUserId(),
                Customer.class, xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorDetails());

        if (session.getUser() != null) {
//			InputStream inputStream = null;
//			byte[] buffered = null;
//
//			try {
//
//				java.util.Properties config = new java.util.Properties();
//				config.put("StrictHostKeyChecking", "no");
//				JSch jsch = new JSch();
//				com.jcraft.jsch.Session session1 = jsch.getSession(EmailTemplates.TABOOR_USER,
//						EmailTemplates.CUSTOMER_MS_HOST, 22);
//				session1.setPassword(EmailTemplates.TABOOR_PASSWORD);
//				session1.setConfig(config);
//				session1.connect();
//				System.out.println("SSh Connected");
//
//				Channel channel = session1.openChannel("sftp");
//				channel.connect();
//
//				ChannelSftp sftp = (ChannelSftp) channel;
//				inputStream = sftp.get(customer.getPictureUrl());
//				if (inputStream != null) {
//					buffered = IOUtils.toByteArray(inputStream);
//				} else {
//					throw new TaboorQMSServiceException(xyzErrorMessage.INPUTSTREAM_IS_NULL.getErrorCode() + "::"
//							+ xyzErrorMessage.INPUTSTREAM_IS_NULL.getErrorDetails());
//				}
//
//				channel.disconnect();
//				session1.disconnect();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}

            return new GetUserResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_FETCHED.getMessage(),
                    session.getUser(), Base64.getEncoder().encodeToString(fileDataService.fetch(new String(
                    Base64.getDecoder().decode(session.getUser().getProfileImageUrl().getBytes())))));
        }

        return new GetUserResponse(xyzResponseCode.ERROR.getCode(), xyzResponseMessage.USER_NOT_EXISTS.getMessage(),
                session.getUser(), "");
    }

    @Override
    public GenStatusResponse deleteUserById(long userId) throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/users/delete?userId=" + userId;
        ResponseEntity<Integer> deleteUserResponse = restTemplate.getForEntity(uri, int.class);

        if (!deleteUserResponse.getStatusCode().equals(HttpStatus.OK) || deleteUserResponse.getBody().equals(0)) {
            return new GenStatusResponse(xyzResponseCode.ERROR.getCode(),
                    xyzResponseMessage.USER_NOT_DELETED.getMessage());
        }
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_DELETED.getMessage());
    }

    @Override
    public boolean verfiyEmailAddress(String encodedPayload) throws TaboorQMSServiceException, Exception {

        String actualPayload = new String(Base64.getDecoder().decode(encodedPayload));
//        logger.info("Actual Payload: " + actualPayload);

        long userId = Long.parseLong(
                actualPayload.substring(actualPayload.indexOf("userId=") + 7, actualPayload.indexOf("&guid=")));
//        logger.info("User ID: " + userId);
        String guid = actualPayload.substring(actualPayload.indexOf("&guid=") + 6);
//        logger.info("Guid: " + guid);

        User user = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL + "/tab/users/get/id?userId=" + userId,
                User.class);

        if (user == null) {
//            logger.info("User Mismatch! No User registered for this Credentials");
            return false;
        } else if (guid.equals(user.getGuid())) {
            user.setEmailVerified(true);
            RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/users/save", user, User.class,
                    xyzErrorMessage.USER_NOT_UPDATED.getErrorCode(),
                    xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());
            return true;
        } else {
//            logger.info("Guid Mismatch! No customer for given Guid");
            return false;
        }
    }

    @Override
    public GenStatusResponse addUserManual(AddUserManualPayload addUserManualPayload,
            @Valid MultipartFile userManualFile) throws TaboorQMSServiceException, Exception {
        UserManual userManual = new UserManual();

        if (addUserManualPayload.getUserManualId() != null) {
            if (addUserManualPayload.getUserManualId() > 0) {
                userManual = RestUtil.getRequest(
                        dbConnectorMicroserviceURL + "/tab/usermanuals/get/id?userManualId="
                        + addUserManualPayload.getUserManualId(),
                        UserManual.class, xyzErrorMessage.USERMANUAL_NOT_CREATED.getErrorCode(),
                        xyzErrorMessage.USERMANUAL_NOT_CREATED.getErrorDetails());
            }
        }
        userManual.setName(addUserManualPayload.getName());

        if (userManualFile != null) {
            if (!userManualFile.isEmpty()) {
                String filePath = TaboorQMSConfigurations.HOST_MANUAL_FOLDER.getValue()
                        .concat(String.valueOf(OffsetDateTime.now().toEpochSecond()))
                        .concat("-" + userManualFile.getOriginalFilename());
                Boolean saved = fileDataService.save(Arrays.asList(new FileDataObject(userManualFile, filePath)));
                if (saved) {
                    userManual.setPath(Base64.getEncoder().encodeToString(filePath.getBytes()));
                }
            }
        }

        String uri = dbConnectorMicroserviceURL + "/tab/usermanuals/save";
        HttpEntity<UserManual> entity = new HttpEntity<>(userManual, HttpHeadersUtils.getApplicationJsonHeader());

        ResponseEntity<UserManual> createUserManualResponse = restTemplate.postForEntity(uri, entity, UserManual.class);

        if (!createUserManualResponse.getStatusCode().equals(HttpStatus.OK)
                || createUserManualResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USERMANUAL_NOT_CREATED.getErrorCode() + "::"
                    + xyzErrorMessage.USERMANUAL_NOT_CREATED.getErrorDetails());
        }
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_MANUAL_ADDED.getMessage());
    }

    @Override
    public GetUserManualListResponse getAllUserManual() throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/usermanuals/get/all";
        ResponseEntity<?> getAllUserManualResponse = restTemplate.getForEntity(uri, List.class);

        List<UserManual> userManualList = GenericMapper.convertListToObject(getAllUserManualResponse.getBody(),
                new TypeReference<List<UserManual>>() {
        });
        if (userManualList == null) {
            userManualList = new ArrayList<UserManual>();
        }

        return new GetUserManualListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_MANUAL_FETCHED.getMessage(), userManualList);
    }

    @Override
    public GenStatusResponse deleteUserManual(long userManaulId) throws TaboorQMSServiceException, Exception {

        Boolean response = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/usermanuals/delete?userManualId=" + userManaulId, Boolean.class,
                xyzErrorMessage.USERMANUAL_NOT_DELETED.getErrorCode(),
                xyzErrorMessage.USERMANUAL_NOT_DELETED.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_MANUAL_DELETED.getMessage());
    }

    @Override
    public GenStatusResponse deleteUserFAQ(long userFAQId) throws TaboorQMSServiceException, Exception {
        Boolean response = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/userfaqs/delete?userFAQId=" + userFAQId, Boolean.class,
                xyzErrorMessage.USERFAQ_NOT_DELETED.getErrorCode(),
                xyzErrorMessage.USERFAQ_NOT_DELETED.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_FAQ_DELETED.getMessage());
    }

    @Override
    public RegisterUserResponse updateUser(RegisterUserPayload registerUserPayload, @Valid MultipartFile profileImage)
            throws TaboorQMSServiceException, Exception {

        if (checkUserExists(registerUserPayload.getEmail()) == true) {
            throw new TaboorQMSServiceException(
                    xyzErrorMessage.USER_EXISTS.getErrorCode() + "::" + xyzErrorMessage.USER_EXISTS.getErrorDetails());
        }
        if (checkUserExistsPhone(registerUserPayload.getPhoneNumber()) == true) {
            throw new TaboorQMSServiceException(
                    xyzErrorMessage.USER_EXISTS_PHONE.getErrorCode() + "::" + xyzErrorMessage.USER_EXISTS_PHONE.getErrorDetails());
        }

        User user = registerUserPayload.getUser();
        if (registerUserPayload.getEmail() != null) {
            user.setEmail(registerUserPayload.getEmail());
        }
        user.setName(registerUserPayload.getName());
//		user.setPassword(Signature.encrypt(registerUserPayload.getPassword(),
//				TaboorQMSConfigurations.PASSWORD_ENCRYPTION_KEY.getValue()));
        user.setPhoneNumber(registerUserPayload.getPhoneNumber());
        if (registerUserPayload.getUserRole() != null) {
            user.setRoleName(registerUserPayload.getUserRole().getRoleName());
        }

        String uri = dbConnectorMicroserviceURL + "/tab/users/save";
        HttpEntity<User> userEntity = new HttpEntity<>(user, HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<User> createUserResponse = restTemplate.postForEntity(uri, userEntity, User.class);

        if (!createUserResponse.getStatusCode().equals(HttpStatus.OK) || createUserResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_UPDATED.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());
        }

        if (registerUserPayload.getUserRole() != null) {
            UserRoleMapper userRoleMapper = RestUtil.get(
                    dbConnectorMicroserviceURL + "/tab/userrolemapper/get/user?userId=" + user.getUserId(),
                    UserRoleMapper.class);
            userRoleMapper.setUserRole(registerUserPayload.getUserRole());

            uri = dbConnectorMicroserviceURL + "/tab/userrolemapper/save";
            HttpEntity<UserRoleMapper> userRoleMapperEntity = new HttpEntity<>(userRoleMapper,
                    HttpHeadersUtils.getApplicationJsonHeader());
            ResponseEntity<UserRoleMapper> createUserRoleMapperResponse = restTemplate.postForEntity(uri,
                    userRoleMapperEntity, UserRoleMapper.class);

            if (registerUserPayload.getAssignRolePrivileges()) {
                List<UserPrivilegeMapper> userPrivilegeMappers = new ArrayList<UserPrivilegeMapper>();

                registerUserPayload.getUserRole().getUserPrivileges().forEach(entity -> {
                    UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
                    userPrivilegeMapper.setUser(createUserResponse.getBody());
                    userPrivilegeMapper.setUserPrivilege(entity);
                    userPrivilegeMappers.add(userPrivilegeMapper);
                });

                uri = dbConnectorMicroserviceURL + "/tab/userprivilegemapper/updateAll";
                HttpEntity<?> httpEntity = new HttpEntity<>(userPrivilegeMappers,
                        HttpHeadersUtils.getApplicationJsonHeader());
                ResponseEntity<Boolean> createUserPrivilegeMappersResponse = restTemplate.postForEntity(uri, httpEntity,
                        Boolean.class);

                if (!createUserPrivilegeMappersResponse.getStatusCode().equals(HttpStatus.OK)
                        || createUserPrivilegeMappersResponse.getBody().equals(false)) {
                    throw new TaboorQMSServiceException(xyzErrorMessage.USERPRIVILEGEMAPPER_NOT_CREATED.getErrorCode()
                            + "::" + xyzErrorMessage.USERPRIVILEGEMAPPER_NOT_CREATED.getErrorDetails());
                }
            }
        }

        return new RegisterUserResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_UPDATED.getMessage(),
                createUserResponse.getBody());
    }

    @Override
    public GenStatusResponse forgetUserPassword(@Valid ResetPasswordPayload resetPasswordPayload)
            throws TaboorQMSServiceException, Exception {
        Pattern pattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
        Matcher mat = pattern.matcher(resetPasswordPayload.getEmail());

        if (mat.matches()) {
            User user = getUserByEmail(resetPasswordPayload.getEmail());
            if (user == null) {
                throw new TaboorQMSServiceException(xyzErrorMessage.PHONE_EMAIL_INCORRECT.getErrorCode() + "::"
                        + xyzErrorMessage.PHONE_EMAIL_INCORRECT.getErrorDetails());
            }
            if (user.getEmailVerified() != null && user.getEmailVerified()) {
                String pass = RandomString.make(10);
                user.setPassword(Signature.encrypt(pass, TaboorQMSConfigurations.PASSWORD_ENCRYPTION_KEY.getValue()));

                RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/users/save",
                        new HttpEntity<>(user, HttpHeadersUtils.getApplicationJsonHeader()), User.class,
                        xyzErrorMessage.USER_NOT_UPDATED.getErrorCode(), xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());

                // Sending Email
                String body = EmailTemplates.EMAIL_TEMPLATE_SIMPLE;
                body = body.replaceAll("NAME", user.getName().toUpperCase());
                body = body.replaceAll("::BODY::", "Your TaboorQMS account's New Password is " + pass);

                SendEmailPayload sendEmailPayload = new SendEmailPayload();
                sendEmailPayload.setBody(body);
                sendEmailPayload.setEmail(user.getEmail());
                sendEmailPayload.setSubject("TaboorQMS User Password Reset");
                RestUtil.postRequest(emailServiceMicroserviceURL + "/email/send",
                        new HttpEntity<>(sendEmailPayload, HttpHeadersUtils.getApplicationJsonHeader()), GenStatusResponse.class,
                        xyzErrorMessage.EMAIL_NOT_SENT.getErrorCode(), xyzErrorMessage.EMAIL_NOT_SENT.getErrorDetails());

//                EmailSending.sendEmail(user.getEmail(), body, "TaboorQMS User Password Reset");
            } else {
                throw new TaboorQMSServiceException(xyzErrorMessage.EMAIL_NOT_VERIFIED.getErrorCode() + "::"
                        + xyzErrorMessage.EMAIL_NOT_VERIFIED.getErrorDetails());
            }

        } else {
            User user = getUserByPhoneNumber(resetPasswordPayload.getEmail());
            if (user == null) {
                throw new TaboorQMSServiceException(xyzErrorMessage.PHONE_EMAIL_INCORRECT.getErrorCode() + "::"
                        + xyzErrorMessage.PHONE_EMAIL_INCORRECT.getErrorDetails());
            }
            if (user.getPhoneVerified() != null && user.getPhoneVerified()) {
                String pass = RandomString.make(10);
                user.setPassword(Signature.encrypt(pass, TaboorQMSConfigurations.PASSWORD_ENCRYPTION_KEY.getValue()));

                RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/users/save",
                        new HttpEntity<>(user, HttpHeadersUtils.getApplicationJsonHeader()), User.class,
                        xyzErrorMessage.USER_NOT_UPDATED.getErrorCode(), xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());

                URI uri = new URI("https://api.smsglobal.com/http-api.php");
                URIBuilder builder = new URIBuilder();
                builder.setScheme(uri.getScheme());
                builder.setHost(uri.getHost());
                builder.setPath(uri.getPath());
                builder.addParameter("action", "sendsms");
                builder.addParameter("user", "pjv9b734");
                builder.addParameter("password", "zadQr4Mw");
                builder.addParameter("from", "Taboor");
                builder.addParameter("to", user.getPhoneNumber());
                builder.addParameter("text", "Your TaboorQMS account's New Password is " + pass);
                HttpGet request = new HttpGet(builder.toString());
                CloseableHttpClient httpClient = HttpClients.createDefault();
                CloseableHttpResponse response = httpClient.execute(request);
            } else {
                throw new TaboorQMSServiceException(xyzErrorMessage.USER_PHONE_NOT_VERIFIED.getErrorCode() + "::"
                        + xyzErrorMessage.USER_PHONE_NOT_VERIFIED.getErrorDetails());
            }
        }
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_UPDATED.getMessage());
    }

    @Override
    public GenStatusResponse updateProfilePicture(MultipartFile profileImage) throws TaboorQMSServiceException {
//      getting session to get logged in user
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        if (session.getUser() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
        }
        User user = session.getUser();
//        logger.info("user: " + user.getUserId());
        String imageURL = user.getProfileImageUrl();
//        If imageURL exists, deleting file
        if (imageURL != null && !imageURL.equals("")) {
            List urlList = new ArrayList<String>();
            urlList.add(new String(Base64.getDecoder().decode(imageURL.getBytes())));
            Boolean fileDeletionResponse = fileDataService.remove(urlList);
        }
        if (!profileImage.isEmpty()) {
            if (!Arrays.asList("image/jpeg", "image/jpg", "image/png").contains(profileImage.getContentType())) {
                throw new TaboorQMSServiceException(xyzErrorMessage.IMAGE_FORMAT_WRONG.getErrorCode() + "::"
                        + xyzErrorMessage.IMAGE_FORMAT_WRONG.getErrorDetails());
            }
            String filePath = TaboorQMSConfigurations.HOST_USER_IMAGE_FOLDER.getValue()
                    .concat(String.valueOf(OffsetDateTime.now().toEpochSecond()).concat(RandomString.make(10)))
                    .concat(profileImage.getOriginalFilename()
                            .substring(profileImage.getOriginalFilename().indexOf(".")));
//                Saving incoming file
            Boolean saved = fileDataService.save(Arrays.asList(new FileDataObject(profileImage, filePath)));
            if (saved) {
//                    If saved setting imageURl of user to the newer encoded path
                user.setProfileImageUrl("");
                user.setProfileImageUrl(Base64.getEncoder().encodeToString(filePath.getBytes()));
//                    saving user
                String uri = dbConnectorMicroserviceURL + "/tab/users/save";
                HttpEntity<User> userEntity = new HttpEntity<>(user, HttpHeadersUtils.getApplicationJsonHeader());
                ResponseEntity<User> createUserResponse = restTemplate.postForEntity(uri, userEntity, User.class);
                if (!createUserResponse.getStatusCode().equals(HttpStatus.OK) || createUserResponse.getBody() == null) {
                    throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_UPDATED.getErrorCode() + "::"
                            + xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());
                }
            } else {
                throw new TaboorQMSServiceException(xyzErrorMessage.FILE_NOT_SAVED.getErrorCode() + "::"
                        + xyzErrorMessage.FILE_NOT_SAVED.getErrorDetails());
            }
        } else {
            throw new TaboorQMSServiceException(xyzErrorMessage.FILE_IS_EMPTY.getErrorCode() + "::"
                    + xyzErrorMessage.FILE_IS_EMPTY.getErrorDetails());
        }
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.PROFILE_PICTURE_UPDATED.getMessage());
    }

}
