package com.taboor.qms.user.management.serviceImpl;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Notification;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetNotificationByUserPayload;
import com.taboor.qms.core.payload.GetUserNotificationPayload;
import com.taboor.qms.core.payload.LoginUserPayload;
import com.taboor.qms.core.payload.PushUserNotificationPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAllUserNotificationsResponse;
import com.taboor.qms.core.response.GetIdListResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.NotificationSender;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.user.management.service.NotificationService;

@Service
public class NotificationServiceImpl implements NotificationService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Override
	public GenStatusResponse pushUserNotication(PushUserNotificationPayload pushUserNotificationPayload)
			throws FirebaseMessagingException, TaboorQMSServiceException {

		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		GsonBuilder gsonMapBuilder = new GsonBuilder();
		Gson gsonObject = gsonMapBuilder.create();

		Notification notification = new Notification();
		notification.setTitle(pushUserNotificationPayload.getTitle());
		notification.setMessage(pushUserNotificationPayload.getDescription());
		notification.setReadStatus(false);
		notification.setCreatedAt(OffsetDateTime.now().withNano(0));
		notification.setUser(session.getUser());
		notification.setMetaData(gsonObject.toJson(pushUserNotificationPayload.getMetaData()));

		RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/notifications/save", notification, Notification.class,
				xyzErrorMessage.NOTIFCATION_NOT_CREATED.getErrorCode(),
				xyzErrorMessage.NOTIFCATION_NOT_CREATED.getErrorDetails());

		GenStatusResponse genStatusResponse = new GenStatusResponse();
		genStatusResponse.setApplicationStatusCode(xyzResponseCode.SUCCESS.getCode());
		genStatusResponse.setApplicationStatusResponse(xyzResponseMessage.NOTIFICATION_CREATED_SUCCESS.getMessage());
		return genStatusResponse;
	}

	@Override
	public GetValuesResposne getUserUnreadCount() throws TaboorQMSServiceException {

		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();

		Integer count = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/notifications/get/count?userId=" + session.getUser().getUserId(),
				Integer.class, xyzErrorMessage.NOTIFCATION_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.NOTIFCATION_NOT_EXISTS.getErrorDetails());

		Map<String, Object> values = new HashMap<String, Object>();
		values.put("NotificationCount", count);

		return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.NOTIFICATION_COUNT_SUCCESS.getMessage(), values);
	}

	@Override
	public GetAllUserNotificationsResponse getUserNotications(GetUserNotificationPayload getUserNotificationPayload)
			throws Throwable {

		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();

		GetNotificationByUserPayload payload = new GetNotificationByUserPayload();
		payload.setLimit(getUserNotificationPayload.getLimit());
		payload.setPageNo(getUserNotificationPayload.getPageNo());
		payload.setUser(session.getUser());

		List<Notification> userNotificationsList = GenericMapper.convertListToObject(
				RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/notifications/get/user", payload, List.class,
						xyzErrorMessage.NOTIFCATION_NOT_EXISTS.getErrorCode(),
						xyzErrorMessage.NOTIFCATION_NOT_EXISTS.getErrorDetails()),
				new TypeReference<List<Notification>>() {
				});

//		Collections.sort(userNotificationsList, Comparator.comparing(Notification::getCreatedAt));
		userNotificationsList.forEach((notification) -> notification.setUser(null));
		GetAllUserNotificationsResponse getUserNotificationsResponse = new GetAllUserNotificationsResponse(
				xyzResponseCode.SUCCESS.getCode(), "User Notifications Fetched Successfully", userNotificationsList);

		return getUserNotificationsResponse;
	}

	@Override
	public GetIdListResponse updateNotificationReadStatus(
			@Valid GetByIdListPayload updateUserNotificationReadStatusPayload)
			throws TaboorQMSServiceException, JsonGenerationException, JsonMappingException, IOException {

		List<Long> notificationIds = GenericMapper
				.convertListToObject(
						RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/notifications/update/readstatus",
								updateUserNotificationReadStatusPayload, List.class,
								xyzErrorMessage.NOTIFICATION_NOT_UPDATED.getErrorCode(),
								xyzErrorMessage.NOTIFICATION_NOT_UPDATED.getErrorDetails()),
						new TypeReference<List<Long>>() {
						});

		GetIdListResponse getIdListResponse = new GetIdListResponse();
		getIdListResponse.setIds(notificationIds);
		getIdListResponse.setApplicationStatusCode(xyzResponseCode.SUCCESS.getCode());
		getIdListResponse.setApplicationStatusResponse(xyzResponseMessage.NOTIFICATION_UPDATED_SUCCESS.getMessage());
		return getIdListResponse;
	}

	@Override
	public GenStatusResponse testUserNotication(LoginUserPayload loginUserPayload)
			throws FirebaseMessagingException, TaboorQMSServiceException {

		Map<String, String> dataMeta = new HashMap<String, String>();
		dataMeta.put("test1", "value1");
		dataMeta.put("test2", "value2");
		dataMeta.put("test3", "value3");

		NotificationSender.sendPushNotificationData("Test Title", "This is a test notification taboor",
				loginUserPayload.getDeviceToken(), dataMeta);

		GenStatusResponse genStatusResponse = new GenStatusResponse();
		genStatusResponse.setApplicationStatusCode(xyzResponseCode.SUCCESS.getCode());
		genStatusResponse.setApplicationStatusResponse(xyzResponseMessage.NOTIFICATION_CREATED_SUCCESS.getMessage());
		return genStatusResponse;
	}

	@Override
	public GetAllUserNotificationsResponse getUserEmailNotications(
			GetUserNotificationPayload getUserNotificationPayload) throws FirebaseMessagingException,
			TaboorQMSServiceException, JsonGenerationException, JsonMappingException, RestClientException, IOException {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();

		GetNotificationByUserPayload payload = new GetNotificationByUserPayload();
		payload.setLimit(getUserNotificationPayload.getLimit());
		payload.setPageNo(getUserNotificationPayload.getPageNo());
		payload.setUser(session.getUser());

		List<Notification> userNotificationsList = GenericMapper.convertListToObject(
				RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/notifications/get/email/user", payload,
						List.class, xyzErrorMessage.NOTIFCATION_NOT_EXISTS.getErrorCode(),
						xyzErrorMessage.NOTIFCATION_NOT_EXISTS.getErrorDetails()),
				new TypeReference<List<Notification>>() {
				});

		Collections.reverse(userNotificationsList);
		// userNotificationsList.forEach((notification) -> notification.setUser(null));
		GetAllUserNotificationsResponse getUserNotificationsResponse = new GetAllUserNotificationsResponse(
				xyzResponseCode.SUCCESS.getCode(), "User Notifications Fetched Successfully", userNotificationsList);

		return getUserNotificationsResponse;
	}

}
