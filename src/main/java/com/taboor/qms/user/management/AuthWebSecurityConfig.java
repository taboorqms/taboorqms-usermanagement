package com.taboor.qms.user.management;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.taboor.qms.core.auth.CustomAccessDeniedHandler;
import com.taboor.qms.core.auth.JwtAuthenticationEntryPoint;
import com.taboor.qms.core.auth.JwtAuthenticationFilter;
import com.taboor.qms.core.utils.PrivilegeName;

@EnableWebSecurity
class AuthWebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService myUserDetailsService;

	@Autowired
	private JwtAuthenticationFilter jwtAuthenticationFilter;

	@Autowired
	JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	@Autowired
	CustomAccessDeniedHandler customAccessDeniedHandler;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(myUserDetailsService);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and()
				.exceptionHandling().accessDeniedHandler(customAccessDeniedHandler).and()
				// don't create session
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
				.antMatchers(HttpMethod.OPTIONS).permitAll()
				.antMatchers("/h2-console/**", "/swagger-ui.html", "/webjars/**", "/swagger-resources/**", "/v2/**")
				.permitAll().antMatchers("/notification/push")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Branch_Managment.name(),
						PrivilegeName.Guest_Ticket.name(), PrivilegeName.Agent_Serve.name())
				.antMatchers("/notification/user/unread")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Branch_Managment.name(),
						PrivilegeName.Guest_Ticket.name())
				.antMatchers("/notification/user/all")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Branch_Managment.name(),
						PrivilegeName.Guest_Ticket.name(), PrivilegeName.Admin_Panel_View_Only.name())
				.antMatchers("/notification/email/user").hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name())
				.antMatchers("/notification/user/update/readstatus")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Branch_Managment.name(),
						PrivilegeName.Guest_Ticket.name(), PrivilegeName.Admin_Panel_View_Only.name())
				.antMatchers("/user/profile/get").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/otp/verify/new").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/otp/send/new").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/password/change").hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name(), PrivilegeName.Customer_Actions.name())
				.antMatchers("/user/delete").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/user/bankcard/all")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Guest_Ticket.name())
				.antMatchers("/user/feedback/add")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Guest_Ticket.name())
				.antMatchers("/bankcard/add").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/bankcard/update").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/bankcard/get/user").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/bankcard/get/id").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/bankcard/delete/id").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/manual/add").hasAnyRole(PrivilegeName.Manual_Edit.name()).antMatchers("/manual/get/all")
				.hasAnyRole(PrivilegeName.Manual_View.name()).antMatchers("/userRole/get/all")
				.hasAnyRole(PrivilegeName.User_View.name()).antMatchers("/userRole/add")
				.hasAnyRole(PrivilegeName.User_Managment.name()).antMatchers("/userRole/update")
				.hasAnyRole(PrivilegeName.User_Managment.name()).antMatchers("/userRole/delete")
				.hasAnyRole(PrivilegeName.User_Managment.name()).antMatchers("/userRole/getPrivileges")
				.hasAnyRole(PrivilegeName.User_Managment.name()).antMatchers("/manual/delete")
				.hasAnyRole(PrivilegeName.Manual_Edit.name()).antMatchers("/faq/delete")
				.hasAnyRole(PrivilegeName.FAQ_Edit.name()).antMatchers("/faq/add")
				.hasAnyRole(PrivilegeName.FAQ_Edit.name()).antMatchers("/faq/get/all")
				.hasAnyRole(PrivilegeName.FAQ_View.name(),PrivilegeName.Customer_Login.name()).antMatchers("/user/logout")
				.hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name(), PrivilegeName.Customer_Login.name(),
						PrivilegeName.Branch_Managment.name(), PrivilegeName.Agent_Login.name())
				.antMatchers("/fileData/get").hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name())
                                .antMatchers("/fileData/get/many").hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name())
				// ByPass Routes
				.antMatchers("/user/register", "/user/update", "/user/delete/id", "/user/login",
						"/userrole/get/rolename", "/session/create", "/session/refresh", "/otp/send", "/otp/verify",
						"/user/exists", "/faq/type/add", "/faq/type/get/all", "/user/get/email", "/password/reset", "/password/forget",
						"/paymentmethod/get/all", "/paymentmethod/get/id", "/notification/test", "/confirmemailaddress", "/user/updateProfilePicture")
				.permitAll().anyRequest().denyAll();
		httpSecurity.headers().frameOptions().disable();
		httpSecurity.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);

	}

}